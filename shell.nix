let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };
in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable."1.68.0".default.override { extensions = [ "rust-src" ]; })

    # used for test debuging
    cargo-insta

    # for main updator
    python3
    nodejs

    # for update versions
    cargo-semver-checks
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}
