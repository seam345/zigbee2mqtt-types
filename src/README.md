# Generator

## Issues/annoyances

- You need to be in src/generate to run, I was trying to contain all generation specific code to 1 directory but because
  I try to cal the generate.sh script in from rust we now need to be in the generate folder. I could probably fix this
  fairly simply by moving all the shell calls to rust
- We need a second command to test changes made t generate.rs, right now we always pull from master
  zigbee-herdsman-converters but if we want to make changes to generate.rs we should be using the same commit the latest
  zigbe2mtt-types was made on
- Editing itself makes things a little annoying, maybe I should just split the two apart fully or look at workspaces

# versions.toml update

# generated with the following in nushell
#  ls zigbee2mqtt-types/vendors/ | each { |dir| cat $"($dir.name)/Cargo.toml" | from toml | select package.version package.name | echo $"\n($in.package_name) = \"($in.package_version)\"" | save --append src/versions.toml}
# cat src/versions.toml | from toml | update versions.zigbee2mqtt_types_vendor_zen "1.0.0" | to toml | save -f src/versions.toml