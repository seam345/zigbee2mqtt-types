#! /usr/bin/env nix-shell
#! nix-shell -p bash python3 nodejs

# todo move into rust
TEMP=$(mktemp -d)
DIR=$(pwd)
if [ ! -d "zigbee-herdsman-converters" ]; then
  git clone https://github.com/Koenkk/zigbee-herdsman-converters.git
fi
cd zigbee-herdsman-converters
git fetch
# Clean git dir
git clean -fdx > /dev/null
git reset --hard HEAD > /dev/null
# checkout desired branch/commit
git checkout 46d3a1abd8416
#git checkout c6b4c9be4c4521fa5661611a30c05120c5f82c35
# apply patch and output
cp $DIR/src/diff.patch ./
git apply diff.patch
npm install > /dev/null
npm run-script output | sed -e 's/^Map([[:digit:]]\+) //' | grep -vE "^>"