use log::{error, trace};
use semver::Version;
use serde::{Deserialize, Serialize};
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;
use std::fs::{File, ReadDir};
use std::io::Write;
use std::iter::Enumerate;
use std::path::Path;
use std::process::{Command, ExitStatus};
use std::rc::Rc;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex};
use std::{fs, thread};
use zigbee2mqtt_types_generator;
use zigbee2mqtt_types_generator::{Config, VersionToml};

fn main() {
    env_logger::init();
    // load version toml
    let file_path = "src/versions.toml";
    log::trace!("opening file: {}", file_path);

    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    let mut versions_toml: VersionToml = toml::from_str(&contents).unwrap();
    let am_versions_toml = Arc::new(Mutex::new(versions_toml));

    // list out vendors

    let mut bar = progress::Bar::new();

    bar.set_job_title("checking vendors...");
    bar.reach_percent(0);

    let am_bar = Arc::new(Mutex::new(bar));

    let dir_iter = fs::read_dir("./zigbee2mqtt-types/vendors")
        .unwrap()
        .into_iter();
    // let dir_iter = fs::read_dir("./zigbee2mqtt-types/vendors").unwrap().
    let length = dir_iter.count();
    let dir_iter = fs::read_dir("./zigbee2mqtt-types/vendors")
        .unwrap()
        .into_iter();
    let am_dir_iter: Arc<Mutex<ReadDir>> = Arc::new(Mutex::new(dir_iter));
    let mut a_done_count = Arc::new(AtomicUsize::new(0));

    let count = thread::available_parallelism().unwrap().get();
    let threads_to_make = (count - 1).max(1);
    let mut thread_vec = Vec::new();

    trace!("create {threads_to_make} threads");
    for _ in 0..threads_to_make {
        let cam_dir_iter = Arc::clone(&am_dir_iter);
        let cam_bar = Arc::clone(&am_bar);
        let cam_versions_toml = Arc::clone(&am_versions_toml);
        let ca_done_count = Arc::clone(&a_done_count);

        let temp = thread::spawn(move || {
            let mut next = cam_dir_iter.lock().unwrap().next();
            while let Some(path) = next {
                let dir = path.unwrap().path();
                let sconfig_toml = fs::read_to_string(&dir.join("Cargo.toml"))
                    .expect("Should have been able to read the file");
                let mut config_toml: Config = toml::from_str(&sconfig_toml).unwrap();

                let output = run_cargo_semver_checks(&dir);
                update_version_map(output, cam_versions_toml.clone(), config_toml.package.name);

                trace!("Update done count and progress bar");
                let done = ca_done_count.fetch_add(1, Ordering::Relaxed);
                cam_bar
                    .lock()
                    .unwrap()
                    .reach_percent(((done * 100) / length).try_into().unwrap());

                next = cam_dir_iter.lock().unwrap().next();
            }
        });

        thread_vec.push(temp);
    }

    for thread in thread_vec {
        thread.join().expect("Thread panicked");
    }

    dbg!(&am_versions_toml);

    let versions_toml = Arc::try_unwrap(am_versions_toml)
        .unwrap()
        .into_inner()
        .unwrap();
    let toml = toml::to_string(&versions_toml).unwrap();

    let mut output = File::create(file_path).unwrap();
    output.write_all(toml.as_bytes()).unwrap();
}

fn update_version_map(
    output_code: i32,
    version_toml: Arc<Mutex<VersionToml>>,
    package_name: String,
) {
    trace!("updating version map");
    match output_code {
        0 => {
            /*// gets the version, and bumps it by a minor or if not currently existing adds it to the map at version 1
            // todo currently not version 1 ready so I am starting at 0.1.0 and bumping minor
            match version_toml.lock().unwrap().versions.entry(package_name) {
                Entry::Vacant(mut entry) => {
                    entry.insert("0.1.0".to_owned());
                }
                Entry::Occupied(mut entry) => {
                    entry.insert(update_minor_version_string(entry.get()));
                }
            }*/
        }
        1 => {
            // gets the version, and bumps it by a major or if not currently existing adds it to the map at version 1
            // todo currently not version 1 ready so I am starting at 0.1.0 and bumping minor
            match version_toml.lock().unwrap().versions.entry(package_name) {
                Entry::Vacant(mut entry) => {
                    entry.insert("0.1.0".to_owned());
                }
                Entry::Occupied(mut entry) => {
                    entry.insert(update_major_version_string(entry.get()));
                }
            }
        }
        _ => {}
    }
}

fn run_cargo_semver_checks(directory: &Path) -> i32 {
    trace!(
        "run cargo semver checks, dir: {:?}. Current dir is {:?}",
        directory,
        std::env::current_dir().unwrap_or("unknown".into())
    );
    let output = Command::new("cargo-semver-checks")
        .args([
            "semver-checks",
            "check-release",
            "--baseline-rev",
            "031689e", // todo allow to be set
        ])
        .current_dir(directory)
        .output()
        .expect("failed to execute process");
    trace!("{:?}", output);
    let Some(output_code)= &output.status.code() else {
        error!("Failed to get output code output is {:?}", &output);
        return -1;
    };
    trace!("exit code of cargo-semver-checks is {output_code}");
    output_code.clone()
}

// todo while not at 1.0.0 I am updating the minor
fn update_major_version_string(sversion: &String) -> String {
    let mut version = Version::parse(sversion).unwrap();
    version.minor += 1;
    version.patch = 0;
    version.to_string()
}

// todo while not at 1.0.0 I am updating the patch
fn update_minor_version_string(sversion: &String) -> String {
    let mut version = Version::parse(sversion).unwrap();
    version.patch += 1;
    version.to_string()
}
