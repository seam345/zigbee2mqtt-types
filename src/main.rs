mod exposes;

use crate::exposes::{Enums, StructName, Structs};
use indoc::indoc;
use log::{error, warn};
use serde::Deserialize;
use serde_json::Value::Null;
use serde_json::{Map, Value};
use std::collections::BTreeMap;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::process::Command;
use zigbee2mqtt_types_generator::convert_model_to_valid_url;
use zigbee2mqtt_types_generator::uppercase_first_letter;
use zigbee2mqtt_types_generator::VersionToml;

const VERSION_STRING: &str = "0.4.0";
const SUB_CRATE_PREFIX: &str = "zigbee2mqtt_types_vendor_";

#[derive(Deserialize)]
struct CargoToml {
    package: Package,
}
#[derive(Deserialize)]
struct Package {
    name: String,
    repository: String,
}
#[derive(Debug)]
pub enum WorkingDirError {
    Unknown,
}

fn main() {
    env_logger::init();
    match confirm_working_directory() {
        Ok(()) => {}
        Err(_err) => panic!("Not in the expected directory"), // todo add better message
    };

    // todo add 2 checks to makes sure we have npm and python3
    // todo consider asking user as I dont setup any PATH separation

    let cmd = match Command::new("bash").arg("./src/generate.sh").output() {
        Ok(val) => val,
        Err(err) => {
            error!("failed to run generate command, error: {}", err);
            todo!()
        }
    };

    warn!(
        "cmd.err = {}",
        std::str::from_utf8(&cmd.stderr).expect("utf encoded string")
    );

    let string = match std::str::from_utf8(&cmd.stdout) {
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    };

    log::trace!("output of generate command {}", string);

    let vendor_map: std::collections::HashMap<String, (Structs, Enums)> =
        string_to_structs(string.to_string());
    make_files(vendor_map);
}

fn confirm_working_directory() -> Result<(), WorkingDirError> {
    log::trace!("Checking which directory we are in");
    let filename = "Cargo.toml";

    let contents = match fs::read_to_string(filename) {
        Ok(c) => c,
        Err(_) => {
            log::error!("Could not read file `{}`", filename);
            return Err(WorkingDirError::Unknown);
        }
    };

    let data: CargoToml = match toml::from_str(&contents) {
        Ok(d) => d,
        Err(_) => {
            log::error!("Unable to load data from `{}`", filename);
            return Err(WorkingDirError::Unknown);
        }
    };

    if data.package.name != "zigbee2mqtt-types-generator" {
        log::error!(
            "incorrect packages name got: `{}`, expecting: 'zigbee2mqtt-types-generator'",
            data.package.name
        );
        return Err(WorkingDirError::Unknown);
    }

    if data.package.repository != "https://gitlab.com/seam345/zigbee2mqtt-types" {
        log::error!("incorrect repository name got: `{}`, expecting: 'https://gitlab.com/seam345/zigbee2mqtt-types'", data.package.repository);
        return Err(WorkingDirError::Unknown);
    }
    Ok(())
}

fn make_files(vendor_map: std::collections::HashMap<String, (Structs, Enums)>) {
    let mut sorted_array: Vec<(String, (Structs, Enums))> = vendor_map.into_iter().collect();
    sorted_array.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

    let mut cargo_toml_string = "".to_owned()
        + "[package]\n"
        + "name = \"zigbee2mqtt-types\"\n"
        + "version = \""
        + VERSION_STRING
        + "\"\n"
        + "edition = \"2021\"\n"
        + "license-file = \"MIT\"\n"
        + "repository = \"https://gitlab.com/seam345/zigbee2mqtt-types\"\n"
        + "description = \"Struct definitions for zigbee2mqtt json messages.\"\n"
        + "readme = \"README.md\"\n"
        + "documentation = \"https://seam345.gitlab.io/zigbee2mqtt-types\"\n\n";

    generate_vendor_crates(&sorted_array);

    cargo_toml_string = generate_cargo_toml_workspace(&sorted_array, cargo_toml_string);
    cargo_toml_string = generate_cargo_toml_dependencies(&sorted_array, cargo_toml_string);
    cargo_toml_string = generate_cargo_toml_features(&sorted_array, cargo_toml_string);

    let lib_string = generate_lib_file(&sorted_array);

    create_file_and_parents(
        "zigbee2mqtt-types/src/lib.rs".to_owned(),
        &lib_string.into_bytes(),
    );
    create_file_and_parents(
        "zigbee2mqtt-types/Cargo.toml".to_owned(),
        &cargo_toml_string.into_bytes(),
    );
}

fn create_file_and_parents(file_path_string: String, content: &[u8]) {
    let file_path = std::path::Path::new(&file_path_string);

    let prefix = file_path.parent().unwrap();
    std::fs::create_dir_all(prefix).unwrap();

    let mut file = match File::create(file_path) {
        Ok(file) => file,
        Err(err) => {
            dbg!(&file_path_string);
            dbg!(err);
            todo!()
        }
    };
    file.write_all(content).expect("file written");
}

fn generate_vendor_crates(vendor_list: &[(std::string::String, (Structs, Enums))]) {
    let file_path = "src/versions.toml";
    log::trace!("opening file: {}", file_path);

    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    let mut versions_toml: VersionToml = toml::from_str(&contents).unwrap();

    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        let file_path_string = format!("zigbee2mqtt-types/vendors/{}/src/lib.rs", vendor_file);

        //todo rethink this is a very quick hack, to append lines to bottom of lib file
        // at somepoint I probably want to prepend documentation to it aswell
        let vendor_lib_string = exposes::vendor_file_string(&vendor.1 .0, &vendor.0, &vendor.1 .1);

        //     format!(
        //     indoc! {r##"{vendor_lib_string}
        //     #[cfg(all(feature = "last_seen_epoch", feature = "last_seen_iso_8601"))]
        //     compile_error!{{"{double_feature_flag_clash_message}"}}
        //     "##},
        //     vendor_lib_string = &vendor.1.clone(),
        //     double_feature_flag_clash_message = DOUBLE_FEATURE_FLAG_CLASH_MESSAGE
        // );

        create_file_and_parents(file_path_string, &vendor_lib_string.into_bytes());

        let name = format!("{SUB_CRATE_PREFIX}{vendor_file}");
        let version = match versions_toml.versions.get(&name) {
            None => {
                error!("no version for {name} using 0.1.0");
                "0.1.0"
            }
            Some(string) => string.as_str(),
        };
        let cargo_toml_string = format!(
            indoc! {
                r#"[package]
                name = "{name}"
                version = "{version}"
                edition = "2021"

                [dependencies]
                serde_json = "1.0"
                serde = {{ version = "1.0", features = ["derive"] }}
                zigbee2mqtt_types_base_types = {{path = "../../base-types", version = "0.1.0"}}

                [features]
                clone = ["zigbee2mqtt_types_base_types/clone"]
                debug = ["zigbee2mqtt_types_base_types/debug"]
                "#
            },
            name = name,
            version = version
        );

        let file_path_string = format!("zigbee2mqtt-types/vendors/{}/Cargo.toml", vendor_file);
        create_file_and_parents(file_path_string, &cargo_toml_string.into_bytes());

        // todo confirm sub-crate can be used on its own
        let readme_string = format!(
            indoc! {
                "# Zigbee2mqtt types {}
                This is an auto generated sub-crate lib containing concrete types that can deserialise zigbee2MQTT messages form {} devices.
                The main crate is https://crates.io/crates/zigbee2mqtt-types however this sub-crate could be used on its own if you are using zigbee devices by this vendor.
                "
            },
            &vendor.0, &vendor.0
        );

        let file_path_string = format!("zigbee2mqtt-types/vendors/{}/README.md", vendor_file);
        create_file_and_parents(file_path_string, &readme_string.into_bytes());
    }
}

fn generate_cargo_toml_workspace(
    vendor_list: &[(std::string::String, (Structs, Enums))],
    mut cargo_toml_string: String,
) -> String {
    cargo_toml_string = format!("{cargo_toml_string}[workspace]\nmembers = [\n");
    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        cargo_toml_string = format!("{cargo_toml_string} \"vendors/{vendor_file}\",\n");
    }
    cargo_toml_string = format!("{cargo_toml_string} ]\n");
    cargo_toml_string
}

fn generate_cargo_toml_dependencies(
    vendor_list: &[(std::string::String, (Structs, Enums))],
    mut cargo_toml_string: String,
) -> String {
    cargo_toml_string = format!(
        indoc! {
         r#"{cargo_toml_string}
            [dev-dependencies]
            serde_json = "1.0"
            zigbee2mqtt_types_base_types ={{path="base-types", version = "0.1.0"}}
            
            [dependencies]
            serde = {{ version = "1.0", features = ["derive"] }}
            "#
        },
        cargo_toml_string = cargo_toml_string,
    );

    let file_path = "src/versions.toml";
    log::trace!("opening file: {}", file_path);

    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    let mut versions_toml: VersionToml = toml::from_str(&contents).unwrap();

    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        let name = format!("{SUB_CRATE_PREFIX}{vendor_file}");
        let version = match versions_toml.versions.get(&name) {
            None => {
                error!("no version for {name} using 0.1.0");
                "0.1.0"
            }
            Some(string) => string.as_str(),
        };
        cargo_toml_string += &format!(
            "{name} = {{path = \"vendors/{vendor_file}\", optional = true, version = \"{version}\"}}\n"
        );
    }
    cargo_toml_string = format!("{}\n", cargo_toml_string);
    cargo_toml_string
}
fn generate_cargo_toml_features(
    vendor_list: &[(String, (Structs, Enums))],
    mut cargo_toml_string: String,
) -> String {
    cargo_toml_string = format!("{}\n[features]\n", cargo_toml_string);

    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        cargo_toml_string = format!(
            "{cargo_toml_string}{vendor_file} = [\"dep:{SUB_CRATE_PREFIX}{vendor_file}\"]\n"
        );
    }

    log::debug!("Generating all feature");
    cargo_toml_string = format!("{}all_vendors = [", cargo_toml_string);
    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        cargo_toml_string = format!(r#"{cargo_toml_string} "{vendor_file}","#);
    }
    cargo_toml_string = format!("{} ]\n", cargo_toml_string);

    log::debug!("Generating Debug feature, this needs the");
    cargo_toml_string = format!("{}debug = [", cargo_toml_string);
    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        cargo_toml_string =
            format!(r#"{cargo_toml_string} "{SUB_CRATE_PREFIX}{vendor_file}?/debug","#,);
    }
    cargo_toml_string = format!("{} ]\n", cargo_toml_string);

    log::debug!("Generating Clone feature");
    cargo_toml_string = format!("{}clone = [", cargo_toml_string);
    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        cargo_toml_string =
            format!(r#"{cargo_toml_string} "{SUB_CRATE_PREFIX}{vendor_file}?/clone","#,);
    }
    cargo_toml_string = format!("{} ]\n", cargo_toml_string);

    cargo_toml_string
}

fn generate_lib_file(vendor_list: &[(std::string::String, (Structs, Enums))]) -> String {
    let mut lib_string = indoc!(
        r#"//! Static types for Zigbee2MQTT
            //!
            //! Vendors:
            "#
    )
    .to_owned();
    // [acova](../zigbee2mqtt_types_vendor_acova/index.html)

    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        lib_string = format!(
            indoc! {
                r#"{current_lib_string}
                //! - [{vendor_display_name}](../{vendor_crate_name}/index.html)"# // note no newline
            },
            current_lib_string = lib_string,
            vendor_display_name = &vendor.0, // todo DARN this is the same as vendor to filename!, I need to get the raw vendor string, not a small undertaking annoyingly (this will only change 1 crate so shall leave it for now
            vendor_crate_name = SUB_CRATE_PREFIX.to_owned() + &vendor_file,
        );
    }

    for vendor in vendor_list.iter() {
        let vendor_file = vendor_to_filename(&vendor.0);
        lib_string = format!(
            indoc! {
                r#"{current_lib_string}
                #[cfg(feature = "{short_vendor_crate_name}")]
                pub use {full_vendor_crate_name} as {short_vendor_crate_name};"#
            },
            current_lib_string = lib_string,
            full_vendor_crate_name = SUB_CRATE_PREFIX.to_owned() + &vendor_file,
            short_vendor_crate_name = vendor_file,
        );
    }

    lib_string
}

fn vendor_to_filename(vendor: &str) -> String {
    let s = vendor.to_lowercase();

    // let mut split = s.split(char::is_ascii_punctuation);
    let mut split = s.split(&['-', ' ', '/', '@', '\\', '(', ')', '&', '.', 'é', 'ü'][..]);
    let mut s = split.next().unwrap_or("").to_string();
    for value in split {
        s = format!("{}{}{}", s, "_", value);
    }
    s
}

fn string_to_structs(string: String) -> std::collections::HashMap<String, (Structs, Enums)> {
    // let mut result = "use serde::{Deserialize};\n".to_string();

    let vendors: Value = match serde_json::from_str(&string) {
        Ok(val) => val,
        Err(err) => {
            error!("Failed to parse string for struct, string is : {}", string);
            println!("{:?}", err);
            panic!();
        }
    };

    let mut vendor_map: std::collections::HashMap<String, (Structs, Enums)> =
        std::collections::HashMap::new();
    // println!("{:?}", v)
    match vendors {
        Value::Null => unimplemented!(),
        Value::Bool(_) => unimplemented!(),
        Value::Number(_) => unimplemented!(),
        Value::String(_) => unimplemented!(),
        Value::Array(_) => unimplemented!(),
        Value::Object(object) => {
            let mut map: Map<String, Value> = Map::new();
            for device in object.into_iter() {
                if !map.contains_key(
                    device.1[0]
                        .get("model")
                        .expect("exists")
                        .as_str()
                        .expect("string"),
                ) {
                    let (vendor_structs, vendor_enums) = vendor_map
                        .entry(vendor_to_filename(
                            device.1[0]
                                .get("vendor")
                                .expect("exists")
                                .as_str()
                                .expect("string"),
                        ))
                        .or_insert((BTreeMap::new(), BTreeMap::new()));

                    // todo why is this unused?
                    let _struct_string =
                        device_to_struct(device.clone(), vendor_structs, vendor_enums);
                    // if !struct_string.contains("Not yet implementing composite types!") {
                    //     result = format!("{}{}", result, device_to_struct(device.clone()));
                    //     vendor_map.insert(
                    //         vendor_to_filename(
                    //             device.1[0]
                    //                 .get("vendor")
                    //                 .expect("exists")
                    //                 .as_str()
                    //                 .expect("string"),
                    //         ),
                    //         format!("{}{}", vendor_string, device_to_struct(device.clone())),
                    //     );
                    // }
                }

                map.insert(
                    device.1[0]
                        .get("model")
                        .expect("exists")
                        .as_str()
                        .expect("string")
                        .to_string(),
                    Null,
                );
            }
        }
    }
    vendor_map
    // result
}

fn device_to_struct(device: (String, Value), all_structs: &mut Structs, all_enums: &mut Enums) {
    log::debug!("Device {}", device.0);

    if let Some(model) = device.1[0].get("model") {
        let name = model.as_str().expect("string");
        let struct_name = StructName {
            rust_name: convert_model_to_valid_struct_name(name),
            original_name: name.to_owned(),
            description: None,
        };
        log::debug!(
                target: &struct_name.rust_name,
            "Exposes {}",
            serde_json::to_string(&device.1[0]["exposes"]).unwrap()
        );
        // options may be the only other interesting field in the future
        log::trace!(
                target: &struct_name.rust_name,
            "Everything {}",
            serde_json::to_string(&device.1[0]).unwrap()
        );

        let inside_struct = match &device.1[0]["exposes"] {
            Value::Null => {
                warn!(target: &struct_name.rust_name, "Device {} had an empty exposes.", device.0);
                return; // todo not a fan of such a nested return
            }
            Value::Bool(_) => unimplemented!(),
            Value::Number(_) => unimplemented!(),
            Value::String(_) => unimplemented!(),
            Value::Array(array) => array,
            Value::Object(_) => unimplemented!(),
        };

        exposes::exposes_to_struct_vals(inside_struct, struct_name, all_structs, all_enums);
    }
}

fn replace_char_with_string(s: &str, c: char, new_string: &str) -> String {
    let mut split = s.split(c);
    let mut s = split.next().unwrap_or("").to_string();
    for value in split {
        s = format!("{}{}{}", s, new_string, value);
    }
    s
}

fn convert_model_to_valid_struct_name(model: &str) -> String {
    let mut s = model.to_lowercase();

    s = replace_char_with_string(&s, '.', "F");
    s = replace_char_with_string(&s, '_', "U");
    s = replace_char_with_string(&s, '-', "D");
    s.retain(|c| !c.is_ascii_punctuation());
    s.retain(|c| c != ' ');
    format!("Zigbee{}", uppercase_first_letter(&s))
}

#[allow(dead_code)]
fn convert_type_to_valid_enum_name(type_name: &str, struct_name: &str) -> String {
    let mut s = type_name.to_lowercase();

    s.retain(|c| !c.is_ascii_punctuation());
    s.retain(|c| c != ' ');
    format!("{}{}", struct_name, uppercase_first_letter(&s))
}
enum Types {
    Unknown,
    Numeric,
    Binary,
    String,
    Enum,
    Composite, // not implemented
}

impl From<&Value> for Types {
    fn from(item: &Value) -> Self {
        match item {
            Value::Null => Types::Unknown,
            Value::Bool(_) => Types::Binary,
            Value::Number(_) => Types::Numeric,
            Value::String(val) => {
                if val.to_lowercase() == "numeric" {
                    Types::Numeric
                } else if val.to_lowercase() == "binary" {
                    Types::Binary
                } else if val.to_lowercase() == "enum" {
                    Types::Enum
                } else if val.to_lowercase() == "composite" {
                    Types::Composite
                } else if val.to_lowercase() == "text" {
                    Types::String
                } else {
                    Types::Unknown
                }
            }
            Value::Array(_) => todo!(),
            Value::Object(_) => todo!(),
        }
    }
}
/*fn exposes_to_struct_vals(exposes: &[Value], struct_name: &str) -> String {
    // todo this whole function is a mess, come back and sort!
    log::trace!(
        "exposes_to_struct_vals, exposes: {}, struct_name: {}",
        serde_json::to_string(exposes).unwrap(),
        // exposes.serialize(),
        struct_name
    );
    let mut string = "{".to_string();
    let mut enum_string = "".to_string();
    let mut map: Map<String, Value> = Map::new();
    for expose in exposes.iter() {
        let expose_obj = expose.as_object().expect("object");
        if expose_obj.contains_key("name") && expose_obj.contains_key("type") {
            if !map.contains_key(expose["name"].as_str().expect("string")) {
                // todo convert into type instead of all the expects

                match Types::from(&expose["type"]) {
                    Types::Binary => {
                        string = format!(
                            "{}\n    pub {}: bool,",
                            string,
                            expose["name"].as_str().expect("string")
                        )
                    }
                    Types::Numeric => {
                        string = format!(
                            "{}\n    pub {}: f64,",
                            string,
                            expose["name"].as_str().expect("string")
                        )
                    }
                    Types::String => {
                        string = format!(
                            "{}\n    pub {}: String,",
                            string,
                            expose["name"].as_str().expect("string")
                        )
                    }
                    Types::Enum => {
                        let enum_name = convert_type_to_valid_enum_name(
                            match expose_obj.get("name") {
                                Some(val) => val.as_str().expect("string"),
                                None => todo!(),
                            },
                            struct_name,
                        );
                        string = format!(
                            "{}\n    pub {}: {},",
                            string,
                            expose["name"].as_str().expect("string"),
                            enum_name
                        );

                        let string = enum_expose_to_rust_enum(expose_obj, &enum_name);
                        if string.is_empty() {
                            return "".to_string(); // todo this is horrid I need a better solution
                        }
                        enum_string = format!("{}\n{}", enum_string, string);
                    }
                    Types::Composite => {
                        warn!(
                            r#"struct_name: {struct_name}, composite type expose: {:?}"#,
                            expose
                        );
                        string = "Not yet implementing composite types!".to_string()
                    }
                    Types::Unknown => {
                        error!(
                            r#"struct_name: {struct_name}, unimplemented type! expose["type"]: {:?}"#,
                            expose["type"]
                        );
                        todo!()
                    }
                }
            } else {
                return "".to_string(); // todo this is horrid I need a better solution
            }
            map.insert(expose["name"].as_str().expect("string").to_string(), Null);
            // string = format!("{}\n {}: {},", string, expose["name"], expose["type"]);
        }
    }
    format!(
        indoc! {
        r##"{struct_string}
                #[cfg(feature = "last_seen_epoch")]
                pub last_seen: u64,
                #[cfg(feature = "last_seen_iso_8601")]
                pub last_seen: String,
                #[cfg(feature = "elapsed")]
                pub elapsed: u64,
            }}

            {enum_string}

            "##},
        struct_string = string,
        enum_string = enum_string,
    )
}
*/
#[allow(dead_code)]
fn enum_expose_to_rust_enum(expose: &Map<String, Value>, enum_name: &str) -> String {
    let mut map: Map<String, Value> = Map::new();
    let mut enum_string = format!(
        "#[cfg_attr(feature = \"debug\", derive(Debug))]\n#[cfg_attr(feature = \"clone\", derive(Clone))]\n#[derive(PartialEq, Deserialize)]\npub enum {} {{",
        enum_name,
    );
    if !expose["values"].is_array() {
        enum_string = "values is not an array! ffs".to_string();
    }
    let list = expose["values"].as_array().expect("array");
    for value in list.iter() {
        let (enum_name, json_name) = match value.as_str() {
            Some(string) => (
                convert_enum_name_to_valid_enum_type(string),
                string.to_string(),
            ),
            None => match value.as_i64() {
                Some(number) => (format!("Number{}", number), number.to_string()),
                None => {
                    println!("failed to parse {:?} as string or number,", value);
                    unimplemented!();
                }
            },
        };
        if !enum_name.is_empty() {
            if !map.contains_key(&enum_name) {
                // todo look into case consistentcy and maybe use #[serde(alias = "foo")] aswell
                enum_string = format!(
                    "{}\n    #[serde(rename = \"{}\")]\n    {},",
                    enum_string, json_name, enum_name
                );
            }
            map.insert(enum_name, Null);
        }

        // enum_string = format!("{}\n    {},", enum_string, value);
    }

    format!("{}\n}}", enum_string)
}

#[allow(dead_code)]
fn convert_enum_name_to_valid_enum_type(name: &str) -> String {
    let mut s = name.to_lowercase();

    s.retain(|c| !c.is_ascii_punctuation());
    s.retain(|c| c != ' ');
    if s.chars().next().unwrap_or('c').is_numeric() {
        format!("Number{}", s)
    } else {
        uppercase_first_letter(&s)
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_convert_model_to_valid_struct_name() {
        let model = "ZS057-D0Z";
        assert_eq!("ZigbeeZs057Dd0z", convert_model_to_valid_struct_name(model));
        let model = "BF 265";
        assert_eq!("ZigbeeBf265", convert_model_to_valid_struct_name(model));
        let model = "5110.40";
        assert_eq!("Zigbee5110F40", convert_model_to_valid_struct_name(model));
        let model = "MEAZON_BIZY_PLUG";
        assert_eq!(
            "ZigbeeMeazonUbizyUplug",
            convert_model_to_valid_struct_name(model)
        );
        let model = "12501";
        assert_eq!("Zigbee12501", convert_model_to_valid_struct_name(model));
    }

    #[test]
    fn test_convert_model_to_valid_url() {
        let model = "YMF40A RL";
        assert_eq!(
            "https://www.zigbee2mqtt.io/devices/YMF40A_RL.html",
            convert_model_to_valid_url(model)
        );
    }

    #[test]
    fn test_convert_enum_name_to_valid_enum_type() {
        let name = "10";
        assert_eq!("Number10", convert_enum_name_to_valid_enum_type(name));

        let name = "10k";
        assert_eq!("Number10k", convert_enum_name_to_valid_enum_type(name));
    }
    /*
        #[test]
        fn test_string_to_structs() {
            // atm this creates two identical enums, so for now just produce nothing
            let json = "{\"lumi.switch.b2nc01\":[{\"zigbeeModel\":[\"lumi.switch.b2nc01\"],\"model\":\"QBKG41LM\",\"vendor\":\"Xiaomi\",\"description\":\"Aqara E1 2 gang switch (with neutral)\",\"fromZigbee\":[{\"cluster\":\"genOnOff\",\"type\":[\"attributeReport\",\"readResponse\"]},{\"cluster\":\"genMultistateInput\",\"type\":[\"attributeReport\"]},{\"cluster\":\"aqaraOpple\",\"type\":[\"attributeReport\",\"readResponse\"]}],\"toZigbee\":[{\"key\":[\"state\",\"on_time\",\"off_wait_time\"]},{\"key\":[\"operation_mode\"]},{\"key\":[\"power_outage_memory\"]},{\"key\":[\"flip_indicator_light\"]},{\"key\":[\"scene_store\"]},{\"key\":[\"scene_recall\"]},{\"key\":[\"scene_add\"]},{\"key\":[\"scene_remove\"]},{\"key\":[\"scene_remove_all\"]},{\"key\":[\"read\"]},{\"key\":[\"write\"]},{\"key\":[\"command\"]}],\"meta\":{\"multiEndpoint\":true},\"exposes\":[{\"type\":\"switch\",\"features\":[{\"type\":\"binary\",\"name\":\"state\",\"property\":\"state_left\",\"access\":7,\"value_on\":\"ON\",\"value_off\":\"OFF\",\"description\":\"On/off state of the switch\",\"value_toggle\":\"TOGGLE\",\"endpoint\":\"left\"}],\"endpoint\":\"left\"},{\"type\":\"switch\",\"features\":[{\"type\":\"binary\",\"name\":\"state\",\"property\":\"state_right\",\"access\":7,\"value_on\":\"ON\",\"value_off\":\"OFF\",\"description\":\"On/off state of the switch\",\"value_toggle\":\"TOGGLE\",\"endpoint\":\"right\"}],\"endpoint\":\"right\"},{\"type\":\"enum\",\"name\":\"operation_mode\",\"property\":\"operation_mode_left\",\"access\":7,\"values\":[\"control_relay\",\"decoupled\"],\"description\":\"Decoupled mode for left button\",\"endpoint\":\"left\"},{\"type\":\"enum\",\"name\":\"operation_mode\",\"property\":\"operation_mode_right\",\"access\":7,\"values\":[\"control_relay\",\"decoupled\"],\"description\":\"Decoupled mode for right button\",\"endpoint\":\"right\"},{\"type\":\"enum\",\"name\":\"action\",\"property\":\"action\",\"access\":1,\"values\":[\"single_left\",\"double_left\",\"single_right\",\"double_right\",\"single_both\",\"double_both\"],\"description\":\"Triggered action (e.g. a button click)\"},{\"type\":\"binary\",\"name\":\"power_outage_memory\",\"property\":\"power_outage_memory\",\"access\":7,\"value_on\":true,\"value_off\":false,\"description\":\"Enable/disable the power outage memory, this recovers the on/off mode after power failure\"},{\"type\":\"numeric\",\"name\":\"device_temperature\",\"property\":\"device_temperature\",\"access\":1,\"unit\":\"°C\",\"description\":\"Temperature of the device\"},{\"type\":\"binary\",\"name\":\"flip_indicator_light\",\"property\":\"flip_indicator_light\",\"access\":7,\"value_on\":\"ON\",\"value_off\":\"OFF\",\"description\":\"After turn on, the indicator light turns on while switch is off, and vice versa\"},{\"type\":\"numeric\",\"name\":\"linkquality\",\"property\":\"linkquality\",\"access\":1,\"unit\":\"lqi\",\"description\":\"Link quality (signal strength)\",\"value_min\":0,\"value_max\":255}],\"ota\":{},\"options\":[{\"type\":\"numeric\",\"name\":\"device_temperature_calibration\",\"property\":\"device_temperature_calibration\",\"access\":2,\"description\":\"Calibrates the device_temperature value (absolute offset), takes into effect on next report of device.\"}]}]}";
            let map = dbg!(string_to_structs(json.to_string()));
            // assert_eq!(
            //     "use serde::{Deserialize, Serialize};\n",
            //     string_to_structs(json.to_string())
            // );

            let json = "{\"spzb0001\":[{\"zigbeeModel\":[\"SPZB0001\"],\"model\":\"SPZB0001\",\"vendor\":\"Eurotronic\",\"description\":\"Spirit Zigbee wireless heater thermostat\",\"fromZigbee\":[{\"cluster\":\"hvacThermostat\",\"type\":[\"attributeReport\",\"readResponse\"],\"options\":[{\"type\":\"binary\",\"name\":\"legacy\",\"property\":\"legacy\",\"access\":2,\"value_on\":true,\"value_off\":false,\"description\":\"Set to false to disable the legacy integration (highly recommended), will change structure of the published payload (default true).\"}]},{\"cluster\":\"genPowerCfg\",\"type\":[\"attributeReport\",\"readResponse\"]}],\"toZigbee\":[{\"key\":[\"occupied_heating_setpoint\"],\"options\":[{\"type\":\"enum\",\"name\":\"thermostat_unit\",\"property\":\"thermostat_unit\",\"access\":2,\"values\":[\"celsius\",\"fahrenheit\"],\"description\":\"Controls the temperature unit of the thermostat (default celsius).\"}]},{\"key\":[\"unoccupied_heating_setpoint\"],\"options\":[{\"type\":\"enum\",\"name\":\"thermostat_unit\",\"property\":\"thermostat_unit\",\"access\":2,\"values\":[\"celsius\",\"fahrenheit\"],\"description\":\"Controls the temperature unit of the thermostat (default celsius).\"}]},{\"key\":[\"local_temperature_calibration\"]},{\"key\":[\"system_mode\"]},{\"key\":[\"eurotronic_host_flags\",\"eurotronic_system_mode\"]},{\"key\":[\"eurotronic_error_status\"]},{\"key\":[\"setpoint_raise_lower\"]},{\"key\":[\"control_sequence_of_operation\"]},{\"key\":[\"remote_sensing\"]},{\"key\":[\"local_temperature\"]},{\"key\":[\"running_state\"]},{\"key\":[\"current_heating_setpoint\"]},{\"key\":[\"eurotronic_trv_mode\",\"trv_mode\"]},{\"key\":[\"eurotronic_valve_position\",\"valve_position\"]},{\"key\":[\"scene_store\"]},{\"key\":[\"scene_recall\"]},{\"key\":[\"scene_add\"]},{\"key\":[\"scene_remove\"]},{\"key\":[\"scene_remove_all\"]},{\"key\":[\"read\"]},{\"key\":[\"write\"]},{\"key\":[\"command\"]}],\"exposes\":[{\"type\":\"numeric\",\"name\":\"battery\",\"property\":\"battery\",\"access\":1,\"unit\":\"%\",\"description\":\"Remaining battery in %\",\"value_min\":0,\"value_max\":100},{\"type\":\"climate\",\"features\":[{\"type\":\"numeric\",\"name\":\"occupied_heating_setpoint\",\"property\":\"occupied_heating_setpoint\",\"access\":7,\"value_min\":5,\"value_max\":30,\"value_step\":0.5,\"unit\":\"°C\",\"description\":\"Temperature setpoint\"},{\"type\":\"numeric\",\"name\":\"local_temperature\",\"property\":\"local_temperature\",\"access\":5,\"unit\":\"°C\",\"description\":\"Current temperature measured on the device\"},{\"type\":\"enum\",\"name\":\"system_mode\",\"property\":\"system_mode\",\"access\":7,\"values\":[\"off\",\"auto\",\"heat\"],\"description\":\"Mode of this device\"},{\"type\":\"enum\",\"name\":\"running_state\",\"property\":\"running_state\",\"access\":5,\"values\":[\"idle\",\"heat\"],\"description\":\"The current running state\"},{\"type\":\"numeric\",\"name\":\"local_temperature_calibration\",\"property\":\"local_temperature_calibration\",\"access\":7,\"value_min\":-30,\"value_max\":30,\"value_step\":0.1,\"unit\":\"°C\",\"description\":\"Offset to be used in the local_temperature\"},{\"type\":\"numeric\",\"name\":\"pi_heating_demand\",\"property\":\"pi_heating_demand\",\"access\":1,\"value_min\":0,\"value_max\":100,\"unit\":\"%\",\"description\":\"Position of the valve (= demanded heat) where 0% is fully closed and 100% is fully open\"}]},{\"type\":\"enum\",\"name\":\"trv_mode\",\"property\":\"trv_mode\",\"access\":7,\"values\":[1,2],\"description\":\"Select between direct control of the valve via the `valve_position` or automatic control of the valve based on the `current_heating_setpoint`. For manual control set the value to 1, for automatic control set the value to 2 (the default). When switched to manual mode the display shows a value from 0 (valve closed) to 100 (valve fully open) and the buttons on the device are disabled.\"},{\"type\":\"numeric\",\"name\":\"valve_position\",\"property\":\"valve_position\",\"access\":7,\"value_min\":0,\"value_max\":255,\"description\":\"Directly control the radiator valve when `trv_mode` is set to 1. The values range from 0 (valve closed) to 255 (valve fully open)\"},{\"type\":\"numeric\",\"name\":\"linkquality\",\"property\":\"linkquality\",\"access\":1,\"unit\":\"lqi\",\"description\":\"Link quality (signal strength)\",\"value_min\":0,\"value_max\":255}],\"ota\":{},\"options\":[{\"type\":\"enum\",\"name\":\"thermostat_unit\",\"property\":\"thermostat_unit\",\"access\":2,\"values\":[\"celsius\",\"fahrenheit\"],\"description\":\"Controls the temperature unit of the thermostat (default celsius).\"},{\"type\":\"binary\",\"name\":\"legacy\",\"property\":\"legacy\",\"access\":2,\"value_on\":true,\"value_off\":false,\"description\":\"Set to false to disable the legacy integration (highly recommended), will change structure of the published payload (default true).\"}]}]}";
            let result = "".to_owned()
                + "use serde::{Deserialize};\n"
                + "// https://www.zigbee2mqtt.io/devices/SPZB0001.html\n"
                + "// vendor: Eurotronic\n"
                + "// Spirit Zigbee wireless heater thermostat\n"
                + "#[cfg_attr(feature = \"debug\", derive(Debug))]\n"
                + "#[derive(Deserialize)]\n"
                + "pub struct ZigbeeSpzb0001 {\n"
                + "    pub battery: i32,\n"
                + "    pub trv_mode: ZigbeeSpzb0001Trvmode,\n"
                + "    pub valve_position: i32,\n"
                + "    pub linkquality: i32,\n"
                + "}\n\n"
                + "#[derive(PartialEq, Deserialize, Debug)]\n"
                + "pub enum ZigbeeSpzb0001Trvmode {\n"
                + "    #[serde(rename = \"1\")]\n"
                + "    Number1,\n"
                + "    #[serde(rename = \"2\")]\n"
                + "    Number2,\n"
                + "}\n\n\n\n";
            let map = dbg!(string_to_structs(json.to_string()));
            assert_eq!(&result, map.get("eurotronic").expect("exists"));
            // assert_eq!(result, string_to_structs(json.to_string()));
            // assert!(false);
        }
    */
    #[test]
    fn test_vendor_to_filename() {
        let vendor = "Eaton/Halo LED";
        assert_eq!("eaton_halo_led", vendor_to_filename(vendor));
        let vendor = "Custom devices (DiY)";
        assert_eq!("custom_devices__diy_", vendor_to_filename(vendor));
        let vendor = "Villeroy & Boch";
        assert_eq!("villeroy___boch", vendor_to_filename(vendor));
        let vendor = "J.XUAN";
        assert_eq!("j_xuan", vendor_to_filename(vendor));
        // the bellow is just removing non ascii chars I would personally prefer to swap them for similar ascii but for now I will _
        let vendor = "Müller Licht";
        assert_eq!("m_ller_licht", vendor_to_filename(vendor));
        let vendor = "Sinopé";
        assert_eq!("sinop_", vendor_to_filename(vendor));
    }
}
