---
source: src/exposes.rs
expression: "vendor_file_string(&empty_map, \"philips\", &enum_map)"
---
'''
use serde::Deserialize;
use serde::de::Unexpected;
use serde::de;
use serde::Deserializer;
use zigbee2mqtt_types_base_types::LastSeen;
/// philips:temp-_ [zigbee2mqtt link](https://www.zigbee2mqtt.io/devices/temp-_.html)
///
/// 
#[cfg_attr(feature = "debug", derive(Debug))]
#[cfg_attr(feature = "clone", derive(Clone))]
#[derive(Deserialize)]
pub struct tempDU {
    ///Brightness of this light
    pub brightness: f64,
    ///Color temperature of this light
    pub color_temp: f64,
    ///Color temperature after cold power on of this light
    pub color_temp_startup: f64,
    ///Link quality (signal strength)
    pub linkquality: f64,
    ///Zigbee herdsman description: "On/off state of this light"
    ///The string values get converted into boolean with: ON = true and OFF = false
    #[serde(deserialize_with = "tempdu_state_deserializer")]
    pub state: bool,
    /// Optional last_seen type, set as a global zigbee2mqtt setting
    pub last_seen: Option<LastSeen>,
    /// Optional elapsed type
    pub elapsed: Option<u64>,
}
/// Deserialize bool from String with custom value mapping
fn tempdu_state_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match String::deserialize(deserializer)?.as_ref() {
        "ON" => Ok(true),
        "OFF" => Ok(false),
        other => Err(de::Error::invalid_value(
            Unexpected::Str(other),
            &"Value expected was either ON or OFF",
        )),
    }
}


#[cfg(all(feature = "last_seen_epoch", feature = "last_seen_iso_8601"))]
compile_error!{"Feature last_seen epoch and iso_8601 are mutually exclusive and cannot be enabled together.
This was done because it is a global setting in zigbee2mqtt and therefor can't see a reason both would be enabled.
If you have a any reason to have both ways enabled please submit an issue to https://gitlab.com/seam345/zigbee2mqtt-types/-/issues"}
'''
