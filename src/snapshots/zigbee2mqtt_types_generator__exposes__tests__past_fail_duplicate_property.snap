---
source: src/exposes.rs
expression: "&empty_map"
---
{
    StructName {
        rust_name: "tempDU",
        original_name: "temp-_",
        description: None,
    }: {
        "alarm_airtemp_overvalue": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Room temperature alarm threshold, between 20 and 60 in °C.  0 means disabled.  Default: 45.",
            ),
        },
        "away_mode": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_away_mode_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_away_mode_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"ON\" => Ok(true),\n        \"OFF\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either ON or OFF\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enable/disable away mode\"\nThe string values get converted into boolean with: ON = true and OFF = false",
            ),
        },
        "button_vibration_level": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUButtonvibrationlevel",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Key beep volume and vibration level.  Default: Low.",
            ),
        },
        "child_lock": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_child_lock_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_child_lock_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"LOCK\" => Ok(true),\n        \"UNLOCK\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either LOCK or UNLOCK\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enables/disables physical input on the device\"\nThe string values get converted into boolean with: LOCK = true and UNLOCK = false",
            ),
        },
        "current": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Instantaneous measured electrical current",
            ),
        },
        "display_auto_off_enabled": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUDisplayautooffenabled",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: None,
        },
        "dry_time": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The duration of Dry Mode, between 5 and 100 minutes.  Default: 5.",
            ),
        },
        "energy": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Sum of consumed energy",
            ),
        },
        "floor_sensor_calibration": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The tempearatue calibration for the exernal floor sensor, between -3 and 3 in 0.1°C.  Default: 0.",
            ),
        },
        "floor_sensor_type": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUFloorsensortype",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Type of the external floor sensor.  Default: NTC 10K/25.",
            ),
        },
        "hysterersis": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Hysteresis setting, between 0.5 and 2 in 0.1 °C.  Default: 0.5.",
            ),
        },
        "lcd_brightness": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDULcdbrightness",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "OLED brightness when operating the buttons.  Default: Medium.",
            ),
        },
        "linkquality": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Link quality (signal strength)",
            ),
        },
        "local_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Current temperature measured on the device",
            ),
        },
        "local_temperature_calibration": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Offset to be used in the local_temperature",
            ),
        },
        "mode_after_dry": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUModeafterdry",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The mode after Dry Mode.  Default: Auto.",
            ),
        },
        "occupied_heating_setpoint": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Temperature setpoint",
            ),
        },
        "outdoor_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Current temperature measured from the floor sensor",
            ),
        },
        "power": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Instantaneous measured power",
            ),
        },
        "powerup_status": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUPowerupstatus",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The mode after a power reset.  Default: Previous Mode.",
            ),
        },
        "running_state": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDURunningstate",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The current running state",
            ),
        },
        "sensor": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUSensor",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The sensor used for heat control.  Default: Room Sensor.",
            ),
        },
        "system_mode": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUSystemmode",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Mode of this device",
            ),
        },
        "temperature_display": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUTemperaturedisplay",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The temperature on the display.  Default: Room Temperature.",
            ),
        },
        "voltage": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Measured electrical potential value",
            ),
        },
        "window_open_check": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The threshold to detect window open, between 1.5 and 4 in 0.5 °C.  Default: 0 (disabled).",
            ),
        },
    },
}
