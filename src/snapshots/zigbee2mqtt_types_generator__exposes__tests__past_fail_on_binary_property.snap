---
source: src/exposes.rs
expression: "&empty_map"
---
{
    StructName {
        rust_name: "tempDU",
        original_name: "temp-_",
        description: None,
    }: {
        "auto_lock": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_auto_lock_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_auto_lock_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"AUTO\" => Ok(true),\n        \"MANUAL\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either AUTO or MANUAL\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enable/disable auto lock\"\nThe string values get converted into boolean with: AUTO = true and MANUAL = false",
            ),
        },
        "away_mode": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_away_mode_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_away_mode_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"ON\" => Ok(true),\n        \"OFF\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either ON or OFF\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enable/disable away mode\"\nThe string values get converted into boolean with: ON = true and OFF = false",
            ),
        },
        "away_preset_days": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Away preset days",
            ),
        },
        "away_preset_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Away preset temperature",
            ),
        },
        "battery_low": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Zigbee herdsman description: \"Indicates if the battery of this device is almost empty\"\nBoolean values can be an unintuitive way round: value_on = true and value_off = false, consider double checking Zigbee2MQTT to understand what they mean",
            ),
        },
        "boost_time": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Boost time",
            ),
        },
        "child_lock": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_child_lock_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_child_lock_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"LOCK\" => Ok(true),\n        \"UNLOCK\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either LOCK or UNLOCK\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enables/disables physical input on the device\"\nThe string values get converted into boolean with: LOCK = true and UNLOCK = false",
            ),
        },
        "comfort_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Comfort temperature",
            ),
        },
        "current_heating_setpoint": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Temperature setpoint",
            ),
        },
        "eco_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Eco temperature",
            ),
        },
        "force": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUForce",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Force the valve position",
            ),
        },
        "linkquality": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Link quality (signal strength)",
            ),
        },
        "local_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Current temperature measured on the device",
            ),
        },
        "local_temperature_calibration": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Offset to be used in the local_temperature",
            ),
        },
        "max_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Maximum temperature",
            ),
        },
        "min_temperature": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Minimum temperature",
            ),
        },
        "position": TypeField {
            optional: false,
            rust_type: F64,
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Position",
            ),
        },
        "preset": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUPreset",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Mode of this device (similar to system_mode)",
            ),
        },
        "running_state": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDURunningstate",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "The current running state",
            ),
        },
        "system_mode": TypeField {
            optional: false,
            rust_type: Enum(
                "tempDUSystemmode",
            ),
            deserialize_function_name: None,
            deserialize_function: None,
            additional_documentation: Some(
                "Mode of this device, in the `heat` mode the TS0601 will remain continuously heating, i.e. it does not regulate to the desired temperature. If you want TRV to properly regulate the temperature you need to use mode `auto` instead setting the desired temperature.",
            ),
        },
        "valve_detection": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_valve_detection_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_valve_detection_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"ON\" => Ok(true),\n        \"OFF\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either ON or OFF\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "The string values get converted into boolean with: ON = true and OFF = false",
            ),
        },
        "window_detection": TypeField {
            optional: false,
            rust_type: Binary,
            deserialize_function_name: Some(
                "tempdu_window_detection_deserializer",
            ),
            deserialize_function: Some(
                "/// Deserialize bool from String with custom value mapping\nfn tempdu_window_detection_deserializer<'de, D>(deserializer: D) -> Result<bool, D::Error>\nwhere\n    D: Deserializer<'de>,\n{\n    match String::deserialize(deserializer)?.as_ref() {\n        \"ON\" => Ok(true),\n        \"OFF\" => Ok(false),\n        other => Err(de::Error::invalid_value(\n            Unexpected::Str(other),\n            &\"Value expected was either ON or OFF\",\n        )),\n    }\n}\n",
            ),
            additional_documentation: Some(
                "Zigbee herdsman description: \"Enables/disables window detection on the device\"\nThe string values get converted into boolean with: ON = true and OFF = false",
            ),
        },
    },
}
