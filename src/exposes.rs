use indoc::indoc;
use log::{debug, error, info, trace, warn};
use serde_json::Value;
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::fmt::{Display, Formatter};

use crate::uppercase_first_letter;
use serde::{Deserialize, Serialize};

// this holds the data ready to be made into structs and enums
pub type Enums = BTreeMap<EnumName, Vec<EnumField>>; // BTree instead of hash because we want a consistent order between git commits
pub type EnumName = String;

#[derive(Debug, Serialize)]
pub struct EnumField {
    rust_name: String,
    original_name: String,
}

pub type Structs = BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>>; // BTree instead of hash because we want a consistent order between git commits
                                                                               // type StructName = String;
pub type StructFieldName = String;

#[derive(Debug, Clone, Serialize)]
pub struct StructName {
    /// The name used in Rust code, this is the value used for hashing and comparing
    pub(crate) rust_name: String,
    /// This is kept around for documentation strings and allowing to convert to other stuff such as url for zigbe2mqtt documentation
    pub(crate) original_name: String,
    pub(crate) description: Option<String>,
}

impl Ord for StructName {
    fn cmp(&self, other: &Self) -> Ordering {
        self.rust_name.cmp(&other.rust_name)
    }
}

impl PartialEq<Self> for StructName {
    fn eq(&self, other: &Self) -> bool {
        self.rust_name.eq(&other.rust_name)
    }
}

impl Eq for StructName {}

impl PartialOrd for StructName {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.rust_name.partial_cmp(&other.rust_name)
    }

    fn lt(&self, other: &Self) -> bool {
        self.rust_name.lt(&other.rust_name)
    }

    fn le(&self, other: &Self) -> bool {
        self.rust_name.le(&other.rust_name)
    }

    fn gt(&self, other: &Self) -> bool {
        self.rust_name.gt(&other.rust_name)
    }

    fn ge(&self, other: &Self) -> bool {
        self.rust_name.ge(&other.rust_name)
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub struct TypeField {
    optional: bool, // should be optional if not always shown on publish, basically missing the first bit https://github.com/Koenkk/zigbee-herdsman-converters/blob/fc5965ab0b02a016c16fd2c9824f7f2c535fc3a7/lib/exposes.js#L468
    rust_type: RustType,
    // todo consider merging the 2 deserialize fields into a struct as makes no sence them having seperate option wrapping
    deserialize_function_name: Option<String>,
    deserialize_function: Option<String>,
    additional_documentation: Option<String>, // used to help improve doc string for struct
}

/// just returns the type as a string, made sense with the title of the struct, but maybe not when looking at the contents
impl Display for TypeField {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.optional {
            write!(f, r"Option<")?;
        }
        match &self.rust_type {
            RustType::F64 => write!(f, "f64")?,
            RustType::Binary => write!(f, "bool")?,
            RustType::Enum(enum_name) => write!(f, "{enum_name}")?,
            RustType::String => write!(f, "String")?,
        }

        if self.optional {
            write!(f, r">")?;
        }

        Ok(())
    }
}
pub fn all_struct_to_string(structs: &Structs, vendor: &str) -> String {
    let mut string = "".to_owned();
    for struct_map in structs {
        trace!("Add struct documentation string");
        string += &format!(
            indoc! {
            r##"/// {vendor}:{model} [zigbee2mqtt link]({model_url})
            ///
            /// {description}
            "##},
            model = struct_map.0.original_name,
            model_url = crate::convert_model_to_valid_url(&struct_map.0.original_name),
            vendor = vendor,
            description = struct_map.0.description.clone().unwrap_or("".to_owned())
        );
        trace!("Add struct derive and feature headers");
        string += indoc! {
        r##"#[cfg_attr(feature = "debug", derive(Debug))]
            #[cfg_attr(feature = "clone", derive(Clone))]
            #[derive(Deserialize)]
            "##
        };
        trace!("construct struct");
        string += &format!(
            "pub struct {struct_name} {{\n",
            struct_name = struct_map.0.rust_name
        );
        let indent = "    ";
        for struct_field in struct_map.1 {
            if let Some(documentation) = &struct_field.1.additional_documentation {
                trace!("Add struct field documentation");
                for split in documentation.split('\n') {
                    string += &format!(r#"{indent}///{split}{}"#, "\n");
                }
            }
            if let Some(function_name) = &struct_field.1.deserialize_function_name {
                trace!("Add struct special deserialization func");
                string += &format!(r##"{indent}#[serde(deserialize_with = "{function_name}")]"##);
                string += "\n";
            }

            trace!("Add struct field ");
            string += &format!(
                "{indent}pub {field_name}: {field_type},",
                field_name = struct_field.0,
                field_type = struct_field.1
            );
            string += "\n";
        }
        trace!("Add optional fields");
        string += &format!(
            indoc! {
            r##"{indent}/// Optional last_seen type, set as a global zigbee2mqtt setting
                {indent}pub last_seen: Option<LastSeen>,
                {indent}/// Optional elapsed type
                {indent}pub elapsed: Option<u64>,
                "##
            },
            indent = indent,
        );
        string += "}";
        trace!("Add any deserializers");
        for struct_field in struct_map.1 {
            if let Some(function_string) = &struct_field.1.deserialize_function {
                trace!(
                    "Add deserializer: {}",
                    &struct_field
                        .1
                        .deserialize_function_name
                        .clone()
                        .expect("both function and name should be Some")
                );
                string += &format!("\n{function_string}\n");
            }
        }
    }

    string
}

pub fn vendor_file_string(structs: &Structs, vendor: &str, enums: &Enums) -> String {
    // todo not every file will need all these imports but this is something we can optimise out later
    let mut string = indoc! {"use serde::Deserialize;
    use serde::de::Unexpected;
    use serde::de;
    use serde::Deserializer;
    use zigbee2mqtt_types_base_types::LastSeen;
    "}
    .to_owned();

    string += &all_struct_to_string(structs, vendor);
    string += "\n";

    for enum_map in enums {
        string += indoc! {
        r##"#[cfg_attr(feature = "debug", derive(Debug))]
            #[cfg_attr(feature = "clone", derive(Clone))]
            #[derive(Deserialize, PartialEq)]
            "##
        };

        string += &format!("pub enum {enum_name} {{\n", enum_name = enum_map.0);
        let indent = "    ";
        for enum_field in enum_map.1 {
            string += &format!(
                indoc! {
                    r##"{indent}#[serde(rename = "{mqtt_string}")]
                    {indent}{rust_enum_name},
                    "##
                },
                rust_enum_name = enum_field.rust_name,
                mqtt_string = enum_field.original_name,
                indent = indent,
            );
        }
        string += "}\n";
    }

    string += indoc! {
        r##"#[cfg(all(feature = "last_seen_epoch", feature = "last_seen_iso_8601"))]
            compile_error!{"Feature last_seen epoch and iso_8601 are mutually exclusive and cannot be enabled together.
            This was done because it is a global setting in zigbee2mqtt and therefor can't see a reason both would be enabled.
            If you have a any reason to have both ways enabled please submit an issue to https://gitlab.com/seam345/zigbee2mqtt-types/-/issues"}
            "##
    };

    string
}

#[derive(Debug, PartialEq, Clone, Serialize)]
enum RustType {
    F64,
    Binary,
    Enum(String),
    #[allow(dead_code)]
    String,
    // Struct,
}

enum Types {
    Unknown,
    Numeric,
    Binary,
    String,
    Enum,
    Composite, // not implemented
}

impl From<&Value> for Types {
    fn from(item: &Value) -> Self {
        match item {
            Value::Null => Types::Unknown,
            Value::Bool(_) => Types::Binary,
            Value::Number(_) => Types::Numeric,
            Value::String(val) => {
                if val.to_lowercase() == "numeric" {
                    Types::Numeric
                } else if val.to_lowercase() == "binary" {
                    Types::Binary
                } else if val.to_lowercase() == "enum" {
                    Types::Enum
                } else if val.to_lowercase() == "composite" {
                    Types::Composite
                } else if val.to_lowercase() == "text" {
                    Types::String
                } else {
                    Types::Unknown
                }
            }
            Value::Array(_) => todo!(),
            Value::Object(_) => todo!(),
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")] // haha snake case is in snake case
enum ExposeObject {
    Numeric(ExposeNumeric),
    Binary(ExposeBinaryTypes),
    Text(ExposeString),
    Enum(ExposeEnum),
    // todo implement the bellow
    Composite, // not implemented
    List,      // not implemented
    // Custom types, todo if these all have the same features pattern look at a way to make a default instead of custom for every one
    #[serde(alias = "light")]
    #[serde(alias = "cover")]
    #[serde(alias = "switch")]
    #[serde(alias = "lock")]
    #[serde(alias = "climate")]
    #[serde(alias = "fan")]
    Featured {
        features: Vec<ExposeObject>,
    },
    // Light { features: Vec<ExposeObject> },
}
#[derive(Deserialize, Debug)]
struct ExposeNumeric {
    access: u8,
    #[allow(dead_code)]
    name: String,
    description: Option<String>,
    property: String,
    #[allow(dead_code)]
    unit: Option<String>,
    #[allow(dead_code)]
    value_max: Option<f64>,
    #[allow(dead_code)]
    value_min: Option<f64>,
    #[allow(dead_code)]
    value_step: Option<f64>,
}

#[derive(Deserialize, Debug)]
struct ExposeString {
    access: u8,
    #[allow(dead_code)]
    name: String,
    description: Option<String>,
    property: String,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum ExposeBinaryTypes {
    String(ExposeBinary),
    Binary(ExposeBinaryBinary),
    Unknown(ExposeBinaryUnknown),
}
#[derive(Deserialize, Debug)]
struct ExposeBinary {
    access: u8,
    description: Option<String>,
    #[allow(dead_code)]
    name: String,
    property: String,
    #[allow(dead_code)]
    value_toggle: Option<String>,
    // should add these values to the documentation! false = <value_off_string>, true = <value_on_string>
    // Sometimes these are the opposite way rounds to what you expect... I should make sure that gets into documentation
    value_off: String,
    value_on: String,
}

#[derive(Deserialize, Debug)]
struct ExposeBinaryBinary {
    access: u8,
    #[allow(dead_code)]
    name: String,
    description: Option<String>,
    property: String,
    #[allow(dead_code)]
    value_toggle: Option<String>, // toggle may exist so might have to be optional
    // should add these values to the documentation! false = <value_off_string>, true = <value_on_string>
    value_off: bool, // confrim this is false
    value_on: bool,  // confirm this is true
}

#[derive(Deserialize, Debug)]
struct ExposeBinaryUnknown {
    access: u8,
    #[allow(dead_code)]
    name: String,
    #[allow(dead_code)]
    description: Option<String>,
    #[allow(dead_code)]
    property: String,
    #[allow(dead_code)]
    value_toggle: Option<String>,
}

#[derive(Deserialize, Debug)]
struct ExposeEnum {
    access: u8,
    #[allow(dead_code)]
    name: String,
    description: Option<String>,
    property: String,
    values: Vec<EnumValue>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
enum EnumValue {
    String(String),
    Integer(f64), // javascript integer
}

pub fn exposes_to_struct_vals(
    exposes: &[Value],
    struct_name: StructName,
    all_structs: &mut Structs,
    all_enums: &mut Enums,
) {
    debug!(
        target: &struct_name.rust_name,
        "exposes_to_struct_vals, exposes: {}, struct_name: {}",
        serde_json::to_string(exposes).unwrap(),
        // exposes.serialize(),
        &struct_name.rust_name
    );

    for expose in exposes.iter() {
        let expose: ExposeObject = match serde_json::from_str(
            &serde_json::to_string(expose).unwrap(),
        ) {
            Ok(val) => val,
            Err(err) => {
                error!("Error parsing exposes: {expose_json}, struct_name: {struct_name}, error: {error}", expose_json =  serde_json::to_string(exposes).unwrap(), struct_name =  &struct_name.rust_name, error = err);
                panic!();
            }
        };
        debug!(
            target: &struct_name.rust_name,
            "exposes deserialized into {:?}, struct_name: {}",
            expose, &struct_name.rust_name
        );

        expose_object_to_struct_vals(expose, struct_name.clone(), all_structs, all_enums);
        trace!(target: &struct_name.rust_name, "updated all_structs {:?}", all_structs);
    }
}

fn expose_sent_from_coordinator(expose: &ExposeObject) -> bool {
    match expose {
        ExposeObject::Numeric(val) => access_to_published(val.access),
        ExposeObject::Binary(val) => match val {
            ExposeBinaryTypes::String(val) => access_to_published(val.access),
            ExposeBinaryTypes::Binary(val) => access_to_published(val.access),
            ExposeBinaryTypes::Unknown(val) => access_to_published(val.access),
        },
        ExposeObject::Text(val) => access_to_published(val.access),
        ExposeObject::Enum(val) => access_to_published(val.access),
        ExposeObject::Composite => todo!(),
        ExposeObject::List => todo!(),
        ExposeObject::Featured { .. } => unimplemented!(),
    }
}
/// returns true if the value will ever be sent from zigbee2mqtt and false if it never will
///
/// Some values are only used to /set things so are never sent from zigbee2mqtt, we want to avoid
/// adding them to our deserializers
#[allow(dead_code)]
fn value_sent_from_coordinator(access: u8) -> bool {
    access_to_published(access) | access_to_get(access)
}

type PropertyName = String;
fn expose_to_type_field(
    expose: &ExposeObject,
    rust_name: &str,
    _orginal_name: &str,
) -> (PropertyName, TypeField) {
    match expose {
        ExposeObject::Numeric(numeric) => {
            (numeric.property.clone(),
                TypeField {
                    optional: !access_to_published(numeric.access),
                    rust_type: RustType::F64,
                    deserialize_function_name: None,
                    deserialize_function: None,
                    additional_documentation: numeric.description.clone(),
                })
        }
        ExposeObject::Binary(binary_type) => {
            match binary_type {
            ExposeBinaryTypes::String(binary) => {
                let deserialize_function_name =
                    format!("{}_{}_deserializer", rust_name.to_lowercase(), binary.property);
                (

                    binary.property.clone(),
                    TypeField {
                        optional: !access_to_published(binary.access),
                        rust_type: RustType::Binary,
                        deserialize_function_name: Some(deserialize_function_name.clone()),
                        deserialize_function: Some(
                            format!(
                                indoc! {
                                                    r##"/// Deserialize bool from String with custom value mapping
                                                    fn {function_name}<'de, D>(deserializer: D) -> Result<bool, D::Error>
                                                    where
                                                        D: Deserializer<'de>,
                                                    {{
                                                        match String::deserialize(deserializer)?.as_ref() {{
                                                            "{true_string}" => Ok(true),
                                                            "{false_string}" => Ok(false),
                                                            other => Err(de::Error::invalid_value(
                                                                Unexpected::Str(other),
                                                                &"Value expected was either {true_string} or {false_string}",
                                                            )),
                                                        }}
                                                    }}
                                                    "##
                                                },
                                function_name = deserialize_function_name,
                                true_string = binary.value_on,
                                false_string = binary.value_off,
                            )
                        ),
                        additional_documentation: Some(
                            format!(
                                "{zigbee2mqtt_description}The string values get converted into boolean with: {value_on} = true and {value_off} = false",
                                zigbee2mqtt_description= zigbee2mqtt_description(binary.description.clone()),
                                value_on = binary.value_on,
                                value_off = binary.value_off
                            )
                        ),
                    },
                    )
            }
            ExposeBinaryTypes::Binary(binary) => {
                (
                    binary.property.clone(),
                    TypeField {
                        optional: !access_to_published(binary.access),
                        rust_type: RustType::Binary,
                        deserialize_function_name: None,
                        deserialize_function: None,
                        additional_documentation: Some(
                            format!(
                                "{zigbee2mqtt_description}Boolean values can be an unintuitive way round: value_on = {value_on} and value_off = {value_off}, consider double checking Zigbee2MQTT to understand what they mean",
                                value_on = binary.value_on,
                                value_off = binary.value_off,
                                zigbee2mqtt_description = zigbee2mqtt_description(binary.description.clone()),
                            ),
                        ),
                    }
                    )
            }
                ExposeBinaryTypes::Unknown(_binary) => {
                    todo!()
                }
            }}
        ExposeObject::Text(expose_string) => {
            (expose_string.property.clone(),
             TypeField {
                 optional: !access_to_published(expose_string.access),
                 rust_type: RustType::F64,
                 deserialize_function_name: None,
                 deserialize_function: None,
                 additional_documentation: expose_string.description.clone(),
             },)
        }
        ExposeObject::Enum(export_enum) => {
            let enum_name =
                    convert_type_to_valid_enum_name(&export_enum.property, rust_name);
            (
            export_enum.property.clone(),
            TypeField {
                optional: !access_to_published(export_enum.access),
                rust_type: RustType::Enum(enum_name),
                deserialize_function_name: None,
                deserialize_function: None,
                additional_documentation: export_enum.description.clone(),
            },
            )}
        ExposeObject::Composite => {todo!()}
        ExposeObject::List => {todo!()}
        ExposeObject::Featured { .. } => {unimplemented!()}
    }
}

fn expose_object_to_struct_vals(
    expose: ExposeObject,
    struct_name: StructName,
    all_structs: &mut Structs,
    all_enums: &mut Enums,
) {
    // todo convert match to if let
    let expose = match expose {
        ExposeObject::Featured { features } => {
            for feature in features {
                expose_object_to_struct_vals(feature, struct_name.clone(), all_structs, all_enums);
            }
            return;
        }
        ExposeObject::Composite => {
            warn!("ignoring composite");
            return;
        }
        ExposeObject::List => {
            warn!("ignoring list");
            return;
        }
        _ => expose,
    };
    if let ExposeObject::Binary(ExposeBinaryTypes::Unknown(binary)) = &expose {
        warn!("Ignoring binary unknown {:?}", binary);
        return; // todo implement
    }

    let device = all_structs
        .entry(struct_name.clone())
        .or_insert(BTreeMap::new());
    debug!(target: &struct_name.rust_name, "Fetched device from map: {:?}", device);

    let (property_name, field) =
        expose_to_type_field(&expose, &struct_name.rust_name, &struct_name.original_name);
    if expose_sent_from_coordinator(&expose) {
        if let ExposeObject::Enum(export_enum) = expose {
            let enum_name = match &field.rust_type {
                RustType::Enum(enum_name) => enum_name,
                _ => {
                    error!(
                          target: &format!("{}.{}", struct_name.rust_name, &property_name),
                        "expose enum was not parsed into field enum, likely error in expose_to_type_field() "
                    );
                    panic!("Failed to parse");
                }
            };
            enum_expose_to_rust_enum(&export_enum, enum_name, all_enums);
        }
        match device.get(&property_name) {
            None => {
                device.insert(property_name, field);
            }
            Some(previous) => {
                info!(
                    target: &format!("{}.{}", struct_name.rust_name, &property_name),
                    "Got duplicated struct field: previous version : {:?}, new version : {:?}",
                    previous,
                    field
                );
                if *previous != field {
                    error!(
                        target: &format!("{}.{}", struct_name.rust_name, &property_name),
                        "duplicated device fields do not resolved the same!"
                    );
                    todo!("Validate duplication!");
                }
            }
        }
    } else {
        debug!(
            target: &format!("{}.{}", struct_name.rust_name, &property_name),
            "Value only used in /set ignore for deserialization"
        );
    }
}

fn zigbee2mqtt_description(description: Option<String>) -> String {
    match description {
        None => "".to_owned(),
        Some(description) => {
            format!(r#"Zigbee herdsman description: "{description}"{}"#, "\n")
        }
    }
}

fn enum_expose_to_rust_enum(expose_enum: &ExposeEnum, enum_name: &str, all_enums: &mut Enums) {
    let mut enum_variants: Vec<EnumField> = expose_enum
        .values
        .clone()
        .into_iter()
        .map(|name| EnumField {
            rust_name: convert_enum_name_to_valid_enum_type(&name),
            original_name: convert_enum_name_to_orginal_name(&name),
        })
        .collect();

    trace!(
        target: enum_name,
        "Enum variants before dedup {:#?}. Generated for {}",
        enum_variants,
        enum_name
    );

    enum_variants.sort_unstable_by(|a, b| a.rust_name.partial_cmp(&b.rust_name).unwrap());
    enum_variants.dedup_by_key(|variant| variant.rust_name.clone());
    debug!(
        target: enum_name,
        "Enum variants {:#?}. Generated for {}", enum_variants, enum_name
    );

    match all_enums.get(enum_name) {
        None => {
            all_enums.insert(enum_name.into(), enum_variants);
        }
        Some(_current_entry) => {
            todo!("Confirm they are the same or blow up");
        }
    }
}

fn convert_enum_name_to_orginal_name(name: &EnumValue) -> String {
    match name {
        EnumValue::String(name) => name.to_owned(),
        EnumValue::Integer(number) => number.to_string(),
    }
}
fn convert_enum_name_to_valid_enum_type(name: &EnumValue) -> String {
    // todo come back
    match name {
        EnumValue::String(name) => {
            let mut s = name.to_lowercase();

            s = s
                .split('_')
                .fold("".to_owned(), |acc, s| acc + &uppercase_first_letter(s));
            s.retain(|c| !c.is_ascii_punctuation());
            s.retain(|c| c != ' ');

            if s.chars().next().unwrap_or('c').is_numeric() {
                format!("Number{}", s)
            } else {
                if s == "" {
                    s = "AnEmptyString".to_owned(); // Hopefully this is unique enough to not cause issues!
                }
                s
            }
        }
        EnumValue::Integer(number) => {
            format!("Number{}UnparseableForNow", number)
        }
    }
}

fn convert_type_to_valid_enum_name(type_name: &str, struct_name: &str) -> String {
    let mut s = type_name.to_lowercase();

    s.retain(|c| !c.is_ascii_punctuation());
    s.retain(|c| c != ' ');
    format!("{}{}", struct_name, uppercase_first_letter(&s))
}

fn access_to_published(access: u8) -> bool {
    match access {
        0 => unimplemented!("Why would this exist"),
        1 => true,
        2 => false,
        3 => true,
        4 => false,
        5 => true,
        6 => false,
        7 => true,
        _ => {
            unimplemented!("Access was more than 3 bits")
        }
    }
}

#[allow(dead_code)]
fn access_to_set(access: u8) -> bool {
    match access {
        0 => unimplemented!("Why would this exist"),
        1 => false,
        2 => true,
        3 => false,
        4 => true,
        5 => false,
        6 => true,
        7 => true,
        _ => {
            unimplemented!("Access was more than 3 bits")
        }
    }
}

#[allow(dead_code)]
fn access_to_get(access: u8) -> bool {
    match access {
        0 => unimplemented!("Why would this exist"),
        1 => false,
        2 => false,
        3 => false,
        4 => true,
        5 => true,
        6 => true,
        7 => true,
        _ => {
            unimplemented!("Access was more than 3 bits")
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn convert_random_expose() {
        init();
        //<editor-fold desc="json string">
        let json = serde_json::json!([
          {
            "features": [
              {
                "access": 3,
                "name": "state",
                "property": "state",
                "type": "enum",
                "values": [
                  "OPEN",
                  "CLOSE",
                  "STOP"
                ]
              },
              {
                "access": 7,
                "description": "Position of this cover",
                "name": "position",
                "property": "position",
                "type": "numeric",
                "value_max": 100,
                "value_min": 0
              }
            ],
            "type": "cover"
          },
          {
            "access": 1,
            "description": "Remaining battery in %",
            "name": "battery",
            "property": "battery",
            "type": "numeric",
            "unit": "%",
            "value_max": 100,
            "value_min": 0
          },
          {
            "access": 1,
            "description": "Link quality (signal strength)",
            "name": "linkquality",
            "property": "linkquality",
            "type": "numeric",
            "unit": "lqi",
            "value_max": 255,
            "value_min": 0
          }
        ]);
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };

        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor_name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor_name", &enum_map));
    }

    #[test]
    fn convert_philips_light_expose() {
        init();

        //<editor-fold desc="json string">
        let json = serde_json::json!([
          {
            "features": [
              {
                "access": 7,
                "description": "On/off state of this light",
                "name": "state",
                "property": "state",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON",
                "value_toggle": "TOGGLE"
              },
              {
                "access": 7,
                "description": "Brightness of this light",
                "name": "brightness",
                "property": "brightness",
                "type": "numeric",
                "value_max": 254,
                "value_min": 0
              },
              {
                "access": 7,
                "description": "Color temperature of this light",
                "name": "color_temp",
                "presets": [
                  {
                    "description": "Coolest temperature supported",
                    "name": "coolest",
                    "value": 153
                  },
                  {
                    "description": "Cool temperature (250 mireds / 4000 Kelvin)",
                    "name": "cool",
                    "value": 250
                  },
                  {
                    "description": "Neutral temperature (370 mireds / 2700 Kelvin)",
                    "name": "neutral",
                    "value": 370
                  },
                  {
                    "description": "Warm temperature (454 mireds / 2200 Kelvin)",
                    "name": "warm",
                    "value": 454
                  },
                  {
                    "description": "Warmest temperature supported",
                    "name": "warmest",
                    "value": 500
                  }
                ],
                "property": "color_temp",
                "type": "numeric",
                "unit": "mired",
                "value_max": 500,
                "value_min": 153
              },
              {
                "access": 7,
                "description": "Color temperature after cold power on of this light",
                "name": "color_temp_startup",
                "presets": [
                  {
                    "description": "Coolest temperature supported",
                    "name": "coolest",
                    "value": 153
                  },
                  {
                    "description": "Cool temperature (250 mireds / 4000 Kelvin)",
                    "name": "cool",
                    "value": 250
                  },
                  {
                    "description": "Neutral temperature (370 mireds / 2700 Kelvin)",
                    "name": "neutral",
                    "value": 370
                  },
                  {
                    "description": "Warm temperature (454 mireds / 2200 Kelvin)",
                    "name": "warm",
                    "value": 454
                  },
                  {
                    "description": "Warmest temperature supported",
                    "name": "warmest",
                    "value": 500
                  },
                  {
                    "description": "Restore previous color_temp on cold power on",
                    "name": "previous",
                    "value": 65535
                  }
                ],
                "property": "color_temp_startup",
                "type": "numeric",
                "unit": "mired",
                "value_max": 500,
                "value_min": 153
              },
              {
                "description": "Color of this light in the CIE 1931 color space (x/y)",
                "features": [
                  {
                    "access": 7,
                    "name": "x",
                    "property": "x",
                    "type": "numeric"
                  },
                  {
                    "access": 7,
                    "name": "y",
                    "property": "y",
                    "type": "numeric"
                  }
                ],
                "name": "color_xy",
                "property": "color",
                "type": "composite"
              },
              {
                "description": "Color of this light expressed as hue/saturation",
                "features": [
                  {
                    "access": 7,
                    "name": "hue",
                    "property": "hue",
                    "type": "numeric"
                  },
                  {
                    "access": 7,
                    "name": "saturation",
                    "property": "saturation",
                    "type": "numeric"
                  }
                ],
                "name": "color_hs",
                "property": "color",
                "type": "composite"
              }
            ],
            "type": "light"
          },
          {
            "access": 2,
            "description": "Triggers an effect on the light (e.g. make light blink for a few seconds)",
            "name": "effect",
            "property": "effect",
            "type": "enum",
            "values": [
              "blink",
              "breathe",
              "okay",
              "channel_change",
              "finish_effect",
              "stop_effect"
            ]
          },
          {
            "access": 1,
            "description": "Link quality (signal strength)",
            "name": "linkquality",
            "property": "linkquality",
            "type": "numeric",
            "unit": "lqi",
            "value_max": 255,
            "value_min": 0
          }
        ]);
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "philips"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "philips", &enum_map));
    }

    // a lot of exposes binarys are really strings, I spent a lot of time working on that case I don't want to have broke this case
    #[test]
    fn convert_expose_with_binary_binary() {
        init();
        let json = serde_json::json!([
            {
                "access":1,
                "description":"Indicates if the contact is closed (= true) or open (= false)",
                "name":"contact",
                "property":"contact",
                "type":"binary",
                "value_off":true,
                "value_on":false
            },{
                "access":1,
                "description":"Remaining battery in %",
                "name":"battery",
                "property":"battery",
                "type":"numeric",
                "unit":"%",
                "value_max":100,
                "value_min":0
            },{
                "access":1,
                "description":"Voltage of the battery in millivolts",
                "name":"voltage",
                "property":"voltage",
                "type":"numeric",
                "unit":"mV"
            },{"access":1,"description":"Link quality (signal strength)","name":"linkquality","property":"linkquality","type":"numeric","unit":"lqi","value_max":255,"value_min":0}]);

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        println!(
            "{}",
            vendor_file_string(&empty_map, "vendor name", &enum_map)
        );
        insta::assert_debug_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_debug_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
                    [
          {
            "access": 7,
            "name": "device_mode",
            "property": "device_mode",
            "type": "binary",
            "value_off": "pilot_off",
            "value_on": "pilot_on"
          },
          {
            "access": 7,
            "name": "cable_outlet_mode",
            "property": "cable_outlet_mode",
            "type": "enum",
            "values": [
              "comfort",
              "comfort-1",
              "comfort-2",
              "eco",
              "frost_protection",
              "off"
            ]
          },
          {
            "features": [
              {
                "access": 7,
                "description": "Works only when the pilot wire is deactivated",
                "name": "state",
                "property": "state",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON",
                "value_toggle": "TOGGLE"
              }
            ],
            "type": "switch"
          },
          {
            "access": 5,
            "description": "Instantaneous measured power",
            "name": "power",
            "property": "power",
            "type": "numeric",
            "unit": "W"
          },
          {
            "access": 7,
            "description": "Controls the behavior when the device is powered on. Works only when the pilot wire is\n                deactivated",
            "name": "power_on_behavior",
            "property": "power_on_behavior",
            "type": "enum",
            "values": [
              "off",
              "previous",
              "on"
            ]
          },
          {
            "access": 1,
            "description": "Link quality (signal strength)",
            "name": "linkquality",
            "property": "linkquality",
            "type": "numeric",
            "unit": "lqi",
            "value_max": 255,
            "value_min": 0
          }
        ]
                    );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail_text_field() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
                            [
          {
            "access": 1,
            "description": "Remaining battery in %",
            "name": "battery",
            "property": "battery",
            "type": "numeric",
            "unit": "%",
            "value_max": 100,
            "value_min": 0
          },
          {
            "access": 1,
            "description": "Measured temperature value",
            "name": "temperature",
            "property": "temperature",
            "type": "numeric",
            "unit": "°C"
          },
          {
            "access": 1,
            "description": "Indicates whether the device detected occupancy",
            "name": "occupancy",
            "property": "occupancy",
            "type": "binary",
            "value_off": false,
            "value_on": true
          },
          {
            "access": 1,
            "description": "Pin code introduced.",
            "name": "action_code",
            "property": "action_code",
            "type": "numeric"
          },
          {
            "access": 1,
            "description": "Last action transaction number.",
            "name": "action_transaction",
            "property": "action_transaction",
            "type": "numeric"
          },
          {
            "access": 1,
            "description": "Alarm zone. Default value 0",
            "name": "action_zone",
            "property": "action_zone",
            "type": "text"
          },
          {
            "access": 1,
            "description": "Triggered action (e.g. a button click)",
            "name": "action",
            "property": "action",
            "type": "enum",
            "values": [
              "disarm",
              "arm_day_zones",
              "arm_night_zones",
              "arm_all_zones",
              "exit_delay",
              "emergency"
            ]
          },
          {
            "access": 1,
            "description": "Link quality (signal strength)",
            "name": "linkquality",
            "property": "linkquality",
            "type": "numeric",
            "unit": "lqi",
            "value_max": 255,
            "value_min": 0
          }
        ]
                            );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail_duplicate_property() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
                                   [
            {
                "access": 5,
                "description": "Current temperature measured on the device",
                "name": "local_temperature",
                "property": "local_temperature",
                "type": "numeric",
                "unit": "°C"
            },
            {
                "access": 5,
                "description": "Current temperature measured from the floor sensor",
                "name": "outdoor_temperature",
                "property": "outdoor_temperature",
                "type": "numeric",
                "unit": "°C"
            },
            {
                "features": [
                    {
                        "access": 7,
                        "description": "Temperature setpoint",
                        "name": "occupied_heating_setpoint",
                        "property": "occupied_heating_setpoint",
                        "type": "numeric",
                        "unit": "°C",
                        "value_max": 40,
                        "value_min": 0,
                        "value_step": 0.1
                    },
                    {
                        "access": 5,
                        "description": "Current temperature measured on the device",
                        "name": "local_temperature",
                        "property": "local_temperature",
                        "type": "numeric",
                        "unit": "°C"
                    },
                    {
                        "access": 7,
                        "description": "Offset to be used in the local_temperature",
                        "name": "local_temperature_calibration",
                        "property": "local_temperature_calibration",
                        "type": "numeric",
                        "unit": "°C",
                        "value_max": 3,
                        "value_min": -3,
                        "value_step": 0.1
                    },
                    {
                        "access": 7,
                        "description": "Mode of this device",
                        "name": "system_mode",
                        "property": "system_mode",
                        "type": "enum",
                        "values": [
                            "off",
                            "auto",
                            "heat"
                        ]
                    },
                    {
                        "access": 5,
                        "description": "The current running state",
                        "name": "running_state",
                        "property": "running_state",
                        "type": "enum",
                        "values": [
                            "idle",
                            "heat"
                        ]
                    }
                ],
                "type": "climate"
            },
            {
                "access": 7,
                "description": "Enable/disable away mode",
                "name": "away_mode",
                "property": "away_mode",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON"
            },
            {
                "access": 7,
                "description": "Enables/disables physical input on the device",
                "name": "child_lock",
                "property": "child_lock",
                "type": "binary",
                "value_off": "UNLOCK",
                "value_on": "LOCK"
            },
            {
                "access": 1,
                "description": "Instantaneous measured power",
                "name": "power",
                "property": "power",
                "type": "numeric",
                "unit": "W"
            },
            {
                "access": 1,
                "description": "Instantaneous measured electrical current",
                "name": "current",
                "property": "current",
                "type": "numeric",
                "unit": "A"
            },
            {
                "access": 1,
                "description": "Measured electrical potential value",
                "name": "voltage",
                "property": "voltage",
                "type": "numeric",
                "unit": "V"
            },
            {
                "access": 1,
                "description": "Sum of consumed energy",
                "name": "energy",
                "property": "energy",
                "type": "numeric",
                "unit": "kWh"
            },
            {
                "access": 7,
                "description": "OLED brightness when operating the buttons.  Default: Medium.",
                "name": "lcd_brightness",
                "property": "lcd_brightness",
                "type": "enum",
                "values": [
                    "low",
                    "mid",
                    "high"
                ]
            },
            {
                "access": 7,
                "description": "Key beep volume and vibration level.  Default: Low.",
                "name": "button_vibration_level",
                "property": "button_vibration_level",
                "type": "enum",
                "values": [
                    "off",
                    "low",
                    "high"
                ]
            },
            {
                "access": 7,
                "description": "Type of the external floor sensor.  Default: NTC 10K/25.",
                "name": "floor_sensor_type",
                "property": "floor_sensor_type",
                "type": "enum",
                "values": [
                    "10k",
                    "15k",
                    "50k",
                    "100k",
                    "12k"
                ]
            },
            {
                "access": 7,
                "description": "The sensor used for heat control.  Default: Room Sensor.",
                "name": "sensor",
                "property": "sensor",
                "type": "enum",
                "values": [
                    "air",
                    "floor",
                    "both"
                ]
            },
            {
                "access": 7,
                "description": "The mode after a power reset.  Default: Previous Mode.",
                "name": "powerup_status",
                "property": "powerup_status",
                "type": "enum",
                "values": [
                    "default",
                    "last_status"
                ]
            },
            {
                "access": 7,
                "description": "The tempearatue calibration for the exernal floor sensor, between -3 and 3 in 0.1°C.  Default: 0.",
                "name": "floor_sensor_calibration",
                "property": "floor_sensor_calibration",
                "type": "numeric",
                "unit": "°C",
                "value_max": 3,
                "value_min": -3,
                "value_step": 0.1
            },
            {
                "access": 7,
                "description": "The duration of Dry Mode, between 5 and 100 minutes.  Default: 5.",
                "name": "dry_time",
                "property": "dry_time",
                "type": "numeric",
                "unit": "min",
                "value_max": 100,
                "value_min": 5
            },
            {
                "access": 7,
                "description": "The mode after Dry Mode.  Default: Auto.",
                "name": "mode_after_dry",
                "property": "mode_after_dry",
                "type": "enum",
                "values": [
                    "off",
                    "manual",
                    "auto",
                    "away"
                ]
            },
            {
                "access": 7,
                "description": "The temperature on the display.  Default: Room Temperature.",
                "name": "temperature_display",
                "property": "temperature_display",
                "type": "enum",
                "values": [
                    "room",
                    "floor"
                ]
            },
            {
                "access": 7,
                "description": "The threshold to detect window open, between 1.5 and 4 in 0.5 °C.  Default: 0 (disabled).",
                "name": "window_open_check",
                "property": "window_open_check",
                "type": "numeric",
                "unit": "°C",
                "value_max": 4,
                "value_min": 1.5,
                "value_step": 0.5
            },
            {
                "access": 7,
                "description": "Hysteresis setting, between 0.5 and 2 in 0.1 °C.  Default: 0.5.",
                "name": "hysterersis",
                "property": "hysterersis",
                "type": "numeric",
                "unit": "°C",
                "value_max": 2,
                "value_min": 0.5,
                "value_step": 0.1
            },
            {
                "access": 7,
                "name": "display_auto_off_enabled",
                "property": "display_auto_off_enabled",
                "type": "enum",
                "values": [
                    "enabled",
                    "disabled"
                ]
            },
            {
                "access": 7,
                "description": "Room temperature alarm threshold, between 20 and 60 in °C.  0 means disabled.  Default: 45.",
                "name": "alarm_airtemp_overvalue",
                "property": "alarm_airtemp_overvalue",
                "type": "numeric",
                "unit": "°C",
                "value_max": 60,
                "value_min": 20
            },
            {
                "access": 1,
                "description": "Link quality (signal strength)",
                "name": "linkquality",
                "property": "linkquality",
                "type": "numeric",
                "unit": "lqi",
                "value_max": 255,
                "value_min": 0
            }
        ]
                                    );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail_on_binary_property() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
                         [
          {
            "features": [
              {
                "access": 3,
                "description": "Enables/disables physical input on the device",
                "name": "state",
                "property": "child_lock",
                "type": "binary",
                "value_off": "UNLOCK",
                "value_on": "LOCK"
              }
            ],
            "type": "lock"
          },
          {
            "features": [
              {
                "access": 3,
                "description": "Enables/disables window detection on the device",
                "name": "state",
                "property": "window_detection",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON",
                "value_toggle": "TOGGLE"
              }
            ],
            "type": "switch"
          },
          {
            "access": 1,
            "description": "Window open?",
            "name": "window_open",
            "property": "window_open",
            "type": "binary"
          },
          {
            "access": 1,
            "description": "Indicates if the battery of this device is almost empty",
            "name": "battery_low",
            "property": "battery_low",
            "type": "binary",
            "value_off": false,
            "value_on": true
          },
          {
            "features": [
              {
                "access": 3,
                "name": "state",
                "property": "valve_detection",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON",
                "value_toggle": "TOGGLE"
              }
            ],
            "type": "switch"
          },
          {
            "access": 1,
            "description": "Position",
            "name": "position",
            "property": "position",
            "type": "numeric",
            "unit": "%"
          },
          {
            "features": [
              {
                "access": 3,
                "description": "Temperature setpoint",
                "name": "current_heating_setpoint",
                "property": "current_heating_setpoint",
                "type": "numeric",
                "unit": "°C",
                "value_max": 35,
                "value_min": 5,
                "value_step": 0.5
              },
              {
                "access": 1,
                "description": "Current temperature measured on the device",
                "name": "local_temperature",
                "property": "local_temperature",
                "type": "numeric",
                "unit": "°C"
              },
              {
                "access": 3,
                "description": "Mode of this device, in the `heat` mode the TS0601 will remain continuously heating, i.e. it does not regulate to the desired temperature. If you want TRV to properly regulate the temperature you need to use mode `auto` instead setting the desired temperature.",
                "name": "system_mode",
                "property": "system_mode",
                "type": "enum",
                "values": [
                  "heat",
                  "auto",
                  "off"
                ]
              },
              {
                "access": 3,
                "description": "Offset to be used in the local_temperature",
                "name": "local_temperature_calibration",
                "property": "local_temperature_calibration",
                "type": "numeric",
                "unit": "°C",
                "value_max": 9,
                "value_min": -9,
                "value_step": 1
              },
              {
                "access": 3,
                "description": "Mode of this device (similar to system_mode)",
                "name": "preset",
                "property": "preset",
                "type": "enum",
                "values": [
                  "schedule",
                  "manual",
                  "boost",
                  "complex",
                  "comfort",
                  "eco"
                ]
              },
              {
                "access": 1,
                "description": "The current running state",
                "name": "running_state",
                "property": "running_state",
                "type": "enum",
                "values": [
                  "idle",
                  "heat"
                ]
              }
            ],
            "type": "climate"
          },
          {
            "features": [
              {
                "access": 3,
                "description": "Enable/disable auto lock",
                "name": "state",
                "property": "auto_lock",
                "type": "binary",
                "value_off": "MANUAL",
                "value_on": "AUTO"
              }
            ],
            "type": "switch"
          },
          {
            "features": [
              {
                "access": 3,
                "description": "Enable/disable away mode",
                "name": "state",
                "property": "away_mode",
                "type": "binary",
                "value_off": "OFF",
                "value_on": "ON"
              }
            ],
            "type": "switch"
          },
          {
            "access": 3,
            "description": "Away preset days",
            "name": "away_preset_days",
            "property": "away_preset_days",
            "type": "numeric",
            "value_max": 100,
            "value_min": 0
          },
          {
            "access": 3,
            "description": "Boost time",
            "name": "boost_time",
            "property": "boost_time",
            "type": "numeric",
            "unit": "s",
            "value_max": 900,
            "value_min": 0
          },
          {
            "access": 3,
            "description": "Comfort temperature",
            "name": "comfort_temperature",
            "property": "comfort_temperature",
            "type": "numeric",
            "unit": "°C",
            "value_max": 30,
            "value_min": 0
          },
          {
            "access": 3,
            "description": "Eco temperature",
            "name": "eco_temperature",
            "property": "eco_temperature",
            "type": "numeric",
            "unit": "°C",
            "value_max": 35,
            "value_min": 0
          },
          {
            "access": 3,
            "description": "Force the valve position",
            "name": "force",
            "property": "force",
            "type": "enum",
            "values": [
              "normal",
              "open",
              "close"
            ]
          },
          {
            "access": 3,
            "description": "Maximum temperature",
            "name": "max_temperature",
            "property": "max_temperature",
            "type": "numeric",
            "unit": "°C",
            "value_max": 35,
            "value_min": 15
          },
          {
            "access": 3,
            "description": "Minimum temperature",
            "name": "min_temperature",
            "property": "min_temperature",
            "type": "numeric",
            "unit": "°C",
            "value_max": 15,
            "value_min": 1
          },
          {
            "access": 3,
            "description": "Away preset temperature",
            "name": "away_preset_temperature",
            "property": "away_preset_temperature",
            "type": "numeric",
            "unit": "°C",
            "value_max": 35,
            "value_min": -10
          },
          {
            "description": "Schedule MODE ⏱ - In this mode, the device executes a preset week programming temperature time and temperature.",
            "features": [
              {
                "access": 3,
                "description": "Week format user for schedule",
                "name": "week",
                "property": "week",
                "type": "enum",
                "values": [
                  "5+2",
                  "6+1",
                  "7"
                ]
              },
              {
                "access": 3,
                "name": "workdays_schedule",
                "property": "workdays_schedule",
                "type": "text"
              },
              {
                "access": 3,
                "name": "holidays_schedule",
                "property": "holidays_schedule",
                "type": "text"
              }
            ],
            "name": "programming_mode",
            "type": "composite"
          },
          {
            "access": 1,
            "description": "Link quality (signal strength)",
            "name": "linkquality",
            "property": "linkquality",
            "type": "numeric",
            "unit": "lqi",
            "value_max": 255,
            "value_min": 0
          }
        ]
                                            );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail_on_list_property() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
            [
                {"features":
                    [
                        {"access":7,"description":"On/off state of this light","name":"state","property":"state","type":"binary","value_off":"OFF","value_on":"ON","value_toggle":"TOGGLE"},
                        {"access":7,"description":"Brightness of this light","name":"brightness","property":"brightness","type":"numeric","value_max":254,"value_min":0},
                        {"access":7,"description":"Color temperature of this light","name":"color_temp","presets":[{"description":"Coolest temperature supported","name":"coolest","value":153},{"description":"Cool temperature (250 mireds / 4000 Kelvin)","name":"cool","value":250},{"description":"Neutral temperature (370 mireds / 2700 Kelvin)","name":"neutral","value":370},{"description":"Warm temperature (454 mireds / 2200 Kelvin)","name":"warm","value":454},{"description":"Warmest temperature supported","name":"warmest","value":500}],"property":"color_temp","type":"numeric","unit":"mired","value_max":500,"value_min":153},
                        {"access":7,"description":"Color temperature after cold power on of this light","name":"color_temp_startup","presets":[{"description":"Coolest temperature supported","name":"coolest","value":153},{"description":"Cool temperature (250 mireds / 4000 Kelvin)","name":"cool","value":250},{"description":"Neutral temperature (370 mireds / 2700 Kelvin)","name":"neutral","value":370},{"description":"Warm temperature (454 mireds / 2200 Kelvin)","name":"warm","value":454},{"description":"Warmest temperature supported","name":"warmest","value":500},{"description":"Restore previous color_temp on cold power on","name":"previous","value":65535}],"property":"color_temp_startup","type":"numeric","unit":"mired","value_max":500,"value_min":153},
                        {"access":7,"description":"Color of this light in the CIE 1931 color space (x/y)","features":[{"access":7,"name":"x","property":"x","type":"numeric"},{"access":7,"name":"y","property":"y","type":"numeric"}],"name":"color_xy","property":"color","type":"composite"},
                        {"access":7,"description":"Color of this light expressed as hue/saturation","features":[{"access":7,"name":"hue","property":"hue","type":"numeric"},{"access":7,"name":"saturation","property":"saturation","type":"numeric"}],"name":"color_hs","property":"color","type":"composite"}
                    ],
                    "type":"light"},
                {"access":7,"description":"Controls the behavior when the device is powered on after power loss","name":"power_on_behavior","property":"power_on_behavior","type":"enum","values":["off","on","toggle","previous"]},
                {"access":2,"name":"gradient_scene","property":"gradient_scene","type":"enum","values":["blossom","crocus","precious","narcissa","beginnings","first_light","horizon","valley_dawn","sunflare","emerald_flutter","memento","resplendent","scarlet_dream","lovebirds","smitten","glitz_and_glam","promise","ruby_romance","city_of_love","honolulu","savanna_sunset","golden_pond","runy_glow","tropical_twilight","miami","cancun","rio","chinatown","ibiza","osaka","tokyo","motown","fairfax","galaxy","starlight","blood moon","artic_aurora","moonlight","nebula","sundown","blue_lagoon","palm_beach","lake_placid","mountain_breeze","lake_mist","ocean_dawn","frosty_dawn","sunday_morning","emerald_isle","spring_blossom","midsummer_sun","autumn_gold","spring_lake","winter_mountain","midwinter","amber_bloom","lily","painted_sky","winter_beauty","orange_fields","forest_adventure","blue_planet","soho","vapor_wave","magneto","tyrell","disturbia","hal","golden_star","under_the_tree","silent_night","rosy_sparkle","festive_fun","colour_burst","crystalline"]},
                {
                    "access":7,"description":"List of RGB HEX colors",
                    "item_type":{
                        "access":"Color in RGB HEX format (eg #663399)", // WHY JUST WHY!!!
                        "name":"hex",
                        "type":"text"
                    },
                    "length_max":9,
                    "length_min":1,
                    "name":"gradient",
                    "property":"gradient",
                    "type":"list"
                },
                {"access":2,"name":"effect","property":"effect","type":"enum","values":["blink","breathe","okay","channel_change","candle","fireplace","colorloop","sunrise","finish_effect","stop_effect","stop_hue_effect"]},
                {"access":1,"description":"Link quality (signal strength)","name":"linkquality","property":"linkquality","type":"numeric","unit":"lqi","value_max":255,"value_min":0}
            ]
        );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    #[test]
    fn past_fail_on_empty_enum_value() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
            [
                {
                    "access":3,
                    "description":"Start feeding",
                    "name":"feed",
                    "property":"feed",
                    "type":"enum",
                    "values":[
                        "", // omg NOOOOOOOOOOOO
                        "START"
                    ]
                },
                {"access":1,"description":"Feeding source","name":"feeding_source","property":"feeding_source","type":"enum","values":["schedule","manual","remote"]},
                {"access":1,"description":"Feeding size","name":"feeding_size","property":"feeding_size","type":"numeric","unit":"portion"},
                {"access":1,"description":"Portions per day","name":"portions_per_day","property":"portions_per_day","type":"numeric"},
                {"access":1,"description":"Weight per day","name":"weight_per_day","property":"weight_per_day","type":"numeric","unit":"g"},
                {"access":1,"description":"Indicates wether there is an error with the feeder","name":"error","property":"error","type":"binary","value_off":false,"value_on":true},
                {"access":3,"description":"Feeding schedule","item_type":{"access":3,"features":[{"access":3,"name":"days","property":"days","type":"enum","values":["everyday","workdays","weekend","mon","tue","wed","thu","fri","sat","sun","mon-wed-fri-sun","tue-thu-sat"]},{"access":3,"name":"hour","property":"hour","type":"numeric"},{"access":3,"name":"minute","property":"minute","type":"numeric"},{"access":3,"name":"size","property":"size","type":"numeric"}],"name":"dayTime","type":"composite"},"name":"schedule","property":"schedule","type":"list"},
                {"features":[{"access":3,"description":"Led indicator","name":"state","property":"led_indicator","type":"binary","value_off":"OFF","value_on":"ON","value_toggle":"TOGGLE"}],"type":"switch"},
                {"features":[{"access":3,"description":"Enables/disables physical input on the device","name":"state","property":"child_lock","type":"binary","value_off":"UNLOCK","value_on":"LOCK"}],"type":"lock"},
                {"access":3,"description":"Feeding mode","name":"mode","property":"mode","type":"enum","values":["schedule","manual"]},
                {"access":3,"description":"One serving size","name":"serving_size","property":"serving_size","type":"numeric","unit":"portion","value_max":10,"value_min":1},
                {"access":3,"description":"Portion weight","name":"portion_weight","property":"portion_weight","type":"numeric","unit":"g","value_max":20,"value_min":1},
                {"access":1,"description":"Link quality (signal strength)","name":"linkquality","property":"linkquality","type":"numeric","unit":"lqi","value_max":255,"value_min":0}
            ]
        );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        insta::assert_toml_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_toml_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }

    // todo this needs fixing and confirming it works
    /*#[test]
    fn past_fail_on_enum_number() {
        init();
        //<editor-fold desc="json">
        let json = serde_json::json!(
         [
            {
                "access": 1,
                "description": "Remaining battery in %",
                "name": "battery",
                "property": "battery",
                "type": "numeric",
                "unit": "%",
                "value_max": 100,
                "value_min": 0
            },
            {
                "features": [
                    {
                        "access": 7,
                        "description": "Temperature setpoint",
                        "name": "occupied_heating_setpoint",
                        "property": "occupied_heating_setpoint",
                        "type": "numeric",
                        "unit": "°C",
                        "value_max": 30,
                        "value_min": 5,
                        "value_step": 0.5
                    },
                    {
                        "access": 5,
                        "description": "Current temperature measured on the device",
                        "name": "local_temperature",
                        "property": "local_temperature",
                        "type": "numeric",
                        "unit": "°C"
                    },
                    {
                        "access": 7,
                        "description": "Mode of this device",
                        "name": "system_mode",
                        "property": "system_mode",
                        "type": "enum",
                        "values": [
                            "off",
                            "auto",
                            "heat"
                        ]
                    },
                    {
                        "access": 5,
                        "description": "The current running state",
                        "name": "running_state",
                        "property": "running_state",
                        "type": "enum",
                        "values": [
                            "idle",
                            "heat"
                        ]
                    },
                    {
                        "access": 7,
                        "description": "Offset to be used in the local_temperature",
                        "name": "local_temperature_calibration",
                        "property": "local_temperature_calibration",
                        "type": "numeric",
                        "unit": "°C",
                        "value_max": 30,
                        "value_min": -30,
                        "value_step": 0.1
                    },
                    {
                        "access": 1,
                        "description": "Position of the valve (= demanded heat) where 0% is fully closed and 100% is fully open",
                        "name": "pi_heating_demand",
                        "property": "pi_heating_demand",
                        "type": "numeric",
                        "unit": "%",
                        "value_max": 100,
                        "value_min": 0
                    }
                ],
                "type": "climate"
            },
            {
                "access": 7,
                "description": "Select between direct control of the valve via the `valve_position` or automatic control of the valve based on the `current_heating_setpoint`. For manual control set the value to 1, for automatic control set the value to 2 (the default). When switched to manual mode the display shows a value from 0 (valve closed) to 100 (valve fully open) and the buttons on the device are disabled.",
                "name": "trv_mode",
                "property": "trv_mode",
                "type": "enum",
                "values": [
                    1,
                    2
                ]
            },
            {
                "access": 7,
                "description": "Directly control the radiator valve when `trv_mode` is set to 1. The values range from 0 (valve closed) to 255 (valve fully open)",
                "name": "valve_position",
                "property": "valve_position",
                "type": "numeric",
                "value_max": 255,
                "value_min": 0
            },
            {
                "access": 1,
                "description": "Link quality (signal strength)",
                "name": "linkquality",
                "property": "linkquality",
                "type": "numeric",
                "unit": "lqi",
                "value_max": 255,
                "value_min": 0
            }
        ]
                                                    );
        //</editor-fold>

        let mut empty_map: BTreeMap<StructName, BTreeMap<StructFieldName, TypeField>> =
            BTreeMap::new();
        let mut enum_map: BTreeMap<EnumName, Vec<EnumField>> = BTreeMap::new();
        // dbg!(json);
        // todo there must be a better way to get it as an array :D
        let temp = match json {
            Value::Null => {
                unimplemented!()
            }
            Value::Bool(_) => {
                unimplemented!()
            }
            Value::Number(_) => {
                unimplemented!()
            }
            Value::String(_) => {
                unimplemented!()
            }
            Value::Array(val) => val,
            Value::Object(_) => {
                unimplemented!()
            }
        };

        let struct_name = StructName {
            rust_name: "tempDU".to_owned(),
            original_name: "temp-_".to_owned(),
            description: None,
        };
        exposes_to_struct_vals(&temp, struct_name, &mut empty_map, &mut enum_map);

        insta::assert_debug_snapshot!(&empty_map);
        insta::assert_debug_snapshot!(&enum_map);

        println!(
            "{}",
            vendor_file_string(&empty_map, "vendor name", &enum_map)
        );
        insta::assert_debug_snapshot!(all_struct_to_string(&empty_map, "vendor name"));
        insta::assert_debug_snapshot!(vendor_file_string(&empty_map, "vendor name", &enum_map));
    }*/
}
