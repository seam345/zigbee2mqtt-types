use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

pub mod exposes;

#[derive(Deserialize, Serialize, Debug)]
pub struct VersionToml {
    pub versions: BTreeMap<String, String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Config {
    pub package: Package,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Package {
    pub name: String,
    pub version: String,
}

// taken from https://stackoverflow.com/questions/38406793/why-is-capitalizing-the-first-letter-of-a-string-so-convoluted-in-rust
pub fn uppercase_first_letter(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}

pub fn convert_model_to_valid_url(model: &str) -> String {
    let s = String::from(model);

    format!(
        "https://www.zigbee2mqtt.io/devices/{}.html",
        s.replace(' ', "_")
    )
}
