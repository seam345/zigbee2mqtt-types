use serde::{Deserialize};

#[cfg_attr(feature = "debug", derive(Debug))]
#[cfg_attr(feature = "clone", derive(Clone))]
#[derive(Deserialize, PartialEq)]
#[serde(untagged)]
pub enum LastSeen {
    Epoch(u64),
    Iso8601(String),
}